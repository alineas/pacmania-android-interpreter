package me.nina.gameobjects;
 
import java.util.List;
import java.util.Set;

import agame.SQLLine;
import agame.Utils.Position;
import me.nina.gameobjects.Character.PosType;

public class Chapter extends GameObject{  
  
	public Chapter(SQLLine l) { super(l); } 
 
	public Chapter() { 	}

	public Scene startScene() { return Scene.get(line.getInt("scenestart")); }
	public Position positionStart() { 
		try {
			return new Position(line.getString("positionstart"));
		} catch (Exception e) {
			return new Position(200,200,PosType.BOTTOM.getValeur());
		}
	}
 
	public int number() { return line.getInt("number"); }
	 
	public static List<Chapter> getAll() {  return getAll(instance); }
	public static Chapter get(int id) { return get(instance,id); }  
	public Action firstAction() { return Action.get(line.getInt("firstaction")); } 
	@Override public String getFolder() { return null; } 
	@Override GameObject instanciate(SQLLine l) {  return new Chapter(l); } 
	@Override boolean isValidElement(SQLLine l) {  return true; } 
	@Override String getTable() {  return "chapters"; }
	static Chapter instance = new Chapter( );
	@Override GameObject default_instance() {  return instance; }

	@Override
	boolean isValidPossessor(SQLLine l, GameObject possessor) { return false; }
 


}
