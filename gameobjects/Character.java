package me.nina.gameobjects;
  
import java.util.List;
import java.util.Set; 
import com.example.kq7.Main;

import agame.Ashe;
import agame.Ashe.AshType;
import agame.SQLLine;
import agame.Utils;
import agame.Utils.Point; 
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Character.PosType; 

public class Character extends GameObject  {  
	
	private PosType posType = PosType.BOTTOM;
	private CharacterState selectedStat ;
	private Animation currentAnime;  
	public Point feetPosition = null;   
	protected double scale = 1.0; 
	private final int talker,initialState;  

	//###############################################
	protected Character(SQLLine l) { super(l); 
		talker = line.getInt("messagetalker"); 
		initialState = line.getInt("initialstate");
		selectedStat = getInitialState() ; 
	}
	//###############################################
	public Character() { talker=initialState=-1;}//fake
	//###############################################
	public void setCurrentAnime(Animation c,boolean doTheOffsetJob) {if(doTheOffsetJob&&!c.equals(currentAnime)&&feetPosition!=null) {
		int offsetX = (int) ((currentAnime.posTo().x - currentAnime.posFrom().x)*(scale+getSelectedStat().scaleCorrection()));
		int offsetY = (int) ((currentAnime.posTo().y - currentAnime.posFrom().y)*(scale+getSelectedStat().scaleCorrection())); 
		Point pos = new Point(feetX()+offsetX,feetY()+offsetY);
		if(currentAnime.isReversed())pos = new Point(feetX()-offsetX,feetY()-offsetY);
		setPos(pos); }
		this.currentAnime = c; } 
	public CharacterState getSelectedStat() { return selectedStat; } 
	public int feetY() { return feetPosition.y; } 
	public int feetX() { return feetPosition.x; }
	public void setPos(Point pos) { feetPosition=pos;}
	public PosType getPosTyp() { return posType; } 
	public int talker( ) { return talker; }
	//###############################################
	public void setSelectedStat(CharacterState selectedStat ) { 
		try {
			this.getCurrentAnime().unsetIdle();
			setSelectedStat(selectedStat,true); 
		} catch (Exception e) {Main.log(e);
			Utils.log("error in setselectedstat!!!");
			this.selectedStat=this.getInitialState(); 
			setCurrentAnime(Animation.get(selectedStat.movingAnimation( PosType.BOTTOM).line.getId()),false) ;
		}
	}
	//###############################################
	public void setSelectedStat(CharacterState selectedStat,boolean doTheOffsetJob) { try { 
Main .instance.removeAsh(new Ashe(this.selectedStat.line.getId(),AshType.IS_IN_STATE,1));
		this.selectedStat = selectedStat; Utils.log("selstat= "+selectedStat.title());
 
		setCurrentAnime( selectedStat.movingAnimation( getPosTyp()) ,doTheOffsetJob ); 
		 this.getCurrentAnime().unsetIdle();
		 Main .instance.addAsh(new Ashe(selectedStat.line.getId(),AshType.IS_IN_STATE,1));
		 currentAnime.rewindAll();
		 currentAnime.setReversed(false); 
		 currentAnime.run();
	} catch (Exception e) { Utils.log("error in setselectedstat"); } } 
	//###############################################
	public Animation getCurrentAnime() {
		if(currentAnime == null){  
			Utils.log("null anim set, returning to initial!!!");
			this.selectedStat=this.getInitialState() ; 
			this.posType=PosType.BOTTOM;
			setCurrentAnime(Animation.get(selectedStat.movingAnimation( posType).line.getId()),false) ; 
		}
		return currentAnime;
	}
	//###############################################
	public void setPosType(PosType posTyp) {if(posTyp == this.posType)return;  if(posTyp==null) {Utils.log("postype null sent");  return; }  
	Animation old = Animation.get(selectedStat.movingAnimation( getPosTyp()).line.getId () );
	try {
		 
		this.posType = posTyp; 
		setCurrentAnime(Animation.get(selectedStat.movingAnimation( getPosTyp()).line.getId()),true);
		currentAnime.rewindAll();
	} catch (Exception e) {Main.log(e);
		Utils.log("setpostype called with wrong anim value... anim was "+old.title()+" postyp called="+posTyp);
		this.selectedStat=this.getInitialState(); 
		setCurrentAnime(Animation.get(selectedStat.movingAnimation( PosType.BOTTOM).line.getId()),false) ;
	} 
}
	//###############################################
	public static Character get(int id) {  return get (instance,id); }
	public static List<Character> getAll() {  return getAll(instance); } 
	public CharacterState getInitialState() { return CharacterState.get(initialState); } 
	@Override public String getFolder() { return "games/"+Main.gameName+"/character/"+title()+"/"; } 
	@Override GameObject instanciate(SQLLine l) {  return new me.nina.gameobjects.Character(l); } 
	@Override boolean isValidElement(SQLLine l) {  return true; }
	@Override String getTable() { return "character"; }
	static Character instance= new Character( );
	@Override GameObject default_instance() { return instance; }
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { return false; }
	//###############################################
	public enum PosType {
	    RIGHT(0), LEFT(1),BOTTOM(2),TOP(3),BOT_RIGHT(4),BOT_LEFT(5),TOP_LEFT(6),TOP_RIGHT(7),UNUSED(8);
	      
	    private final int valeur; 
	    PosType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; }  
	    public static PosType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static PosType from(int valeur) {
	        for (PosType type : values()) if (type.getValeur() == valeur) return type; 
	        throw new IllegalArgumentException("no postype like " + valeur);
	    }
	    
	    public static PosType calculatePosType(int xin, int yin, int xto, int yto) { 
	        double angle = Math.atan2(yto - yin, xto - xin);
	        double angleDegrees = Math.toDegrees(angle);
	        if (angleDegrees < 0) angleDegrees += 360;

	        if (angleDegrees >= 337.5 || angleDegrees < 22.5) {
	            return PosType.RIGHT;
	        } else if (angleDegrees >= 22.5 && angleDegrees < 67.5) {
	        	return PosType.BOT_RIGHT;
	        } else if (angleDegrees >= 67.5 && angleDegrees < 112.5) {
	        	return PosType.BOTTOM;
	        } else if (angleDegrees >= 112.5 && angleDegrees < 157.5) {
	        	return PosType.BOT_LEFT;
	        } else if (angleDegrees >= 157.5 && angleDegrees < 202.5) {
	        	return PosType.LEFT;
	        } else if (angleDegrees >= 202.5 && angleDegrees < 247.5) {
	        	return PosType.TOP_LEFT;
	        } else if (angleDegrees >= 247.5 && angleDegrees < 292.5) {
	        	return PosType.TOP;
	        } else {
	        	return PosType.TOP_RIGHT;
	        }
	    } 
	} 
}
