package me.nina.gameobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import agame.SQLLine; 
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Animation.AnimationState;

public class BasicObject extends GameObject  { 
	public Animation icon; 
	public ObjectState selectedState;  
	private boolean started;
	private final int zindex;
	private final ArrayList<BasicObject> childs;
	protected final ObjectState initialState;
	private final int type;  
	public final static List<BasicObject> hiddenObjects = new ArrayList<BasicObject>();
	 
	protected BasicObject(SQLLine line) { 
		super(line);   
		zindex=line.getInt("zindex");
		childs = new ArrayList<BasicObject>();
		for(String a:line.getArray("childs") )if(!a.equals(""))
			childs.add(BasicObject.get(Integer.parseInt(a)));
		initialState=ObjectState.get(line.getInt("initialstate"));
		type=line.getInt("type");
	} 
 	
	static GameObject instance = new BasicObject();//fake
	public BasicObject() { zindex=type=-1;childs=null;initialState=null; }
  
	
	
	public int zindex() { return zindex; }
 	public List<BasicObject> childs() { return childs; }
 	public ObjectState initialState() { return initialState; }
 	public ObjectType type() { return ObjectType.from(type); }  
	//###############################################
		public void populate() {   
			icon = Animation.get(getSelectedState().animID());   
			if(!(this instanceof InventoryObject))
				icon.setLocation(getSelectedState().pos().x,getSelectedState().pos().y);
			icon.rewindAll();
			
			if(!started) {
				icon.forwardAll();//at the start, dont play the reversed loop anim
				started=true;
			} 
		}  
	//############################################### 

	public ObjectState getSelectedState() {  if(selectedState == null) selectedState=initialState(); return selectedState; }

	public void setSelectedState(ObjectState selectedState) {
		this.selectedState = selectedState; 
		if(childs().size() != 0) {
			if(Animation.get(selectedState.animID()).state() == AnimationState.INVISIBLE)
				for(BasicObject a : childs())   
					hiddenObjects.add(a); 
			else
				for(BasicObject a : childs())   
					hiddenObjects.remove(a); 
		}
	}
 
	//############################################### 
	@Override String getTable() { return "basicobject"; } 

	@Override public String getFolder() {
		String path = "";   
		if(type() ==ObjectType.GROUND)
			path = possessorObj().getFolder()+"basicobjects/"; 
		return  path ; 
	}

	@Override GameObject default_instance() {  return instance; } 

	@Override GameObject instanciate(SQLLine l) {
		if( l.getInt("type") == ObjectType.GROUND.getValeur())  
			return new BasicObject(l);  
		return new InventoryObject (l);   
	}

	@Override boolean isValidElement(SQLLine l) {
		return l.getInt("type") != ObjectType.INVENTORY.getValeur(); 
	} 

	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) {
		return possessor.line.getId() == l.getInt("possessor") &&
				l.getInt("type") != ObjectType.INVENTORY.getValeur(); //inv have no possessor
	}
	public static List<BasicObject> getObjects(Scene scene) {
		return getbyPossessor(instance , scene);
	}
	public static List<BasicObject> getAll() {return getAll(instance); } 

	public static BasicObject get(int id) {
		BasicObject temp = get(instance,id);
		if(temp!=null)return temp;
		return InventoryObject.get(id);
	}

	public boolean canBeFullscreen() {
		for(ObjectState a : ObjectState.getByObject(line.getId())) { 
			if(a.isFullscreen())
				return true;
		} return false; 
	} 


	public static  enum ObjectType { GROUND(0),INVENTORY(1);  
		private final int valeur;  ObjectType(int v) { valeur = v;  } 
		public int getValeur() {  return valeur; }  
		public static ObjectType from(String s) { return from(Integer.parseInt(s)); }
		public static ObjectType from(int valeur) {
			for (ObjectType type : values())   if (type.getValeur() == valeur) return type; 
			throw new IllegalArgumentException("no objecttype with value " + valeur);
		} 
	}
} 

