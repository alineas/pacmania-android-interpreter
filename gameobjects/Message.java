package me.nina.gameobjects;
 
import java.util.List;
import java.util.Set;

import com.example.kq7.Main;

import agame.SQLLine;

public class Message extends GameObject {
	private final int sierraid,talker,seq,cond,verb,noun; 
	 
	public String getSound(){
		return getFolder()+ "Audio"+sierraid+"-"+noun+"-"+verb+"-"+cond+"-"+seq+".mp3";
	}
	public Message (SQLLine l) {
		super(l);
		sierraid=l.getInt("sierraid");
		noun=l.getInt("noun");
		verb=l.getInt("verb");
		cond=l.getInt("cond");
		seq=l.getInt("seq");
		talker=l.getInt("talker"); 
	}

	public Message() {sierraid=talker=seq=cond=verb=noun=-1; }//fake
   
	public int talker() { if(talker==-998)return -1; return talker ; } 
	public static List<Message> getAlls( ) {return getAll(instance);}
	@Override GameObject instanciate(SQLLine l) {  return new Message(l); }
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { return false; }
	@Override boolean isValidElement(SQLLine l) { return true; }
	@Override public String getTable() { return "message"; }
	static Message instance = new Message();
	@Override GameObject default_instance() {  return instance; }
	public static Message get(int id){ return get(instance, id); }
	@Override public String getFolder() { return "games/"+Main .gameName+"/dialogs/"; }
  
 
}
