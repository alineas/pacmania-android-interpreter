package me.nina.gameobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import agame.SQLLine;

public class Stair extends Zone {

	
	
 
	public List<StairList> primaryZones; 
	public List<StairList> secondaryZones;
	public List<Integer> priorities;
	public int level = 0;
	  
	
	public Stair(String sql,int id) {
		super("0,0,0,0,-1",id);
		primaryZones = new ArrayList<StairList>(); 
		secondaryZones = new ArrayList<StairList>();
		priorities = new ArrayList<Integer>();
		String pzones = sql.split("@")[0];
		String szones = sql.split("@")[1];
		String pris = sql.split("@")[2];
		String lev = sql.split("@")[3]; 
		for(String a : pris.split(","))if(!a.equals(""))
			priorities.add(Integer.parseInt(a));
		for(String a : pzones.split("\\*"))if(!a.equals(""))
			primaryZones.add((StairList) StairList.get(Integer.parseInt(a)));
		for(String a : szones.split("\\*"))if(!a.equals(""))
			secondaryZones.add((StairList) StairList.get(Integer.parseInt(a)));
		level = Integer.parseInt(lev); 
		 
	} 

 
	public Stair() {
		// TODO Auto-generated constructor stub
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + level;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stair other = (Stair) obj;
		if (level != other.level)
			return false;
		return true;
	} 
	public static Stair DEFAULT_INSTANCE=new Stair();
	
	public int getLevel() {
		return level;
	}

 
	 
	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l.getInt("type") == ZoneSort.STAIR.getValeur() && l.getInt("possessor") == owner.line.getId();
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.STAIR.getValeur())
     		return true;
		return false;
	}

static Stair instance = new Stair();
	@Override
	GameObject default_instance() { 
		return instance;
	}
 


	public static List<Zone> getByOwner(int ownerId){
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
 
	
	public static List<Zone> getAll( ) {
	    return getAll(instance);
	}
	public static Zone get( int id) {
	    return get(instance,id);
	}
	
	
	
	
}
