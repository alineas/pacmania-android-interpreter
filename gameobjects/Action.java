package me.nina.gameobjects;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import agame.Ashe;
import agame.SQLLine;

public class Action extends GameObject {   
	//########################################################################
	static Action instance= new Action(); //fake
	public Action() {ashToSet=ashNeeded=ashRefused=null;detectors=null;timer=random=-1; }  
	//########################################################################
	private final LinkedList<Integer> detectors;
	private final LinkedList<Ashe> ashToSet,ashNeeded,ashRefused;
	private final int timer, random;
	//########################################################################
 	private Action(SQLLine line) { 
 		super(line);  
 		ashToSet = Ashe.getlist(line.getString("setash"));
 		ashNeeded = Ashe.getlist(line.getString("neededash"));
 		ashRefused = Ashe.getlist(line.getString("refusedash"));
 		detectors = line.getList("possessor");
 		timer = line.getInt("timer");
 		random = line.getInt("random");
	}
 	//########################################################################
 	public static Action get(int i) { 
 		return get(instance,i); } 
 	public static List<Action> getAll( ) { 
 		return getAll(instance); }
	public static List<Action> getByDetector( GameObject possessor) {  
		return getbyPossessor(instance, possessor); }
	//########################################################################
	public LinkedList <Ashe> ashToSet() { return ashToSet; } 
	public LinkedList <Ashe> ashNeeded() { return ashNeeded; } 
	public LinkedList <Ashe> ashRefused() { return ashRefused; } 
	public LinkedList <Integer> detectors() {return detectors;}  
	public int timer() { return timer; } 
	public int random() { return random; } 
	//########################################################################
	@Override public String getFolder() { return null; }   
	@Override GameObject instanciate(SQLLine l) { return new Action(l); }
	@Override boolean isValidElement(SQLLine l) {  return true; } 
	@Override String getTable() {  return "action"; } 
	@Override GameObject default_instance() {  return instance; } 
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
			return l.getString("possessor").contains( String.valueOf(possessor.line.getId())); } //dont instanciate linked list everytime
	//########################################################################
	public static boolean isDetector(Zone z) {
		for(Action action : getAll())
			if(action.detectors().contains(z.line.getId()))
				return true;
		return false;
	}
} 
  


 