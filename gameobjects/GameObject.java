package me.nina.gameobjects;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.example.kq7.Main;

import agame.SQLLine;
import agame.SQLTable;
import android.util.SparseArray;
import me.nina.gameobjects.GameObject;
 
public abstract class GameObject  { 
 
	final static Map<GameObject, SparseArray<GameObject>> mappe = new HashMap<>();//all the data!
	public final SQLLine line; 
	public final int id;
	public final int possessor;
	public final String img;
    private static Map<String, List<Long>> instantiationTimes = new HashMap<>();
	//###############################################
	public GameObject(SQLLine line){   
		this.line = line; 
		this.id=line.getId(); 
		possessor = line.getInt("possessor");
		img = line.getString("img");
	}  
	//###############################################
	public GameObject() { possessor=id=-1;line=null;img=null;}//fake
	//###############################################
	public static <T extends GameObject> T get( GameObject  instance, int id) {  
		
		if (!mappe.containsKey(instance))  {
            mappe.put(instance, new SparseArray<GameObject>()); 
            instantiationTimes.put(instance.getClass().getSimpleName(), new ArrayList<Long>());
		}
		SparseArray<GameObject> sparseArray = mappe.get(instance); 
		
        if (sparseArray.indexOfKey(id) < 0) { 
			SQLLine element = SQLTable.getTable(instance.getTable()) .getById(id) ;
				if( element!=null&&instance.isValidElement(element)) {
					long l = System.currentTimeMillis();
					GameObject obj = instance.instanciate(element);
					long g = System.currentTimeMillis()-l ;
					total+=g;
			        mappe.get(instance).put(id, obj);
				}
		}
		
        return (T) mappe.get(instance).get(id); 
    }static long total=0;
	//###############################################
	public static  <T extends GameObject> List<T> getAll(GameObject clazz) { Main.log("total "+clazz.getClass().getSimpleName()+" "+total);
		List<GameObject> allGameObjects = new ArrayList<GameObject>(); 
		String tableName = clazz.getTable();
		Main.log("loading "+tableName);
		for (SQLLine l : SQLTable.getTable(tableName).getLines().values()) 
			if(clazz.isValidElement(l))
				allGameObjects.add(get(clazz, l.getId()));
		return (List<T>) allGameObjects;
	}
	//###############################################
	public static  <T extends GameObject> List<T> getbyPossessor(GameObject clazz, GameObject possessor ) {  
		List<GameObject> allGameObjects = new ArrayList<GameObject>();  
		for (SQLLine l : SQLTable.getTable(clazz.getTable()).getLines().values()) 
			if(clazz.isValidPossessor(l, possessor) && clazz.isValidElement(l) )
				allGameObjects.add(get(clazz, l.getId()));
		return (List<T>) allGameObjects;
	}
	//###############################################
	public static GameObject getById(int int1) {
		if(Scene.get(int1) != null)return Scene.get(int1); 
		if(CharacterState.get(int1)!= null)return CharacterState.get(int1);
		if(InventoryObject.get(int1)!= null)return InventoryObject.get(int1);
		if(ObjectState.get(int1)!= null)return ObjectState.get(int1);
		if(Character.get(int1) != null)return Character.get(int1); 
		if(BasicObject.get(int1) != null)return BasicObject.get(int1); 
		if(Message.get(int1) != null)return Message.get(int1); 
		if(Action.get(int1) != null)return Action.get(int1); 
		if(Chapter.get(int1) != null)return Chapter.get(int1); 
		if(Zone.get(int1) != null)return Zone.get(int1); 
		if(TransitionPoint.get(int1) != null)return TransitionPoint.get(int1); 
		if(Stair.get(int1) != null)return Stair.get(int1); 
		if(StairList.get(int1) != null)return StairList.get(int1); 
		if(SpawnPoint.get(int1) != null)return SpawnPoint.get(int1); 
		if(SpecialZoneInv.get(int1) != null)return SpecialZoneInv.get(int1); 
		return null;
	}
	//###############################################
	public String title(){
		String s = line.getString("title");
		if(s.equals(""))s="no title defined";
		return s;}
	//###############################################
    abstract GameObject instanciate(SQLLine l); 
	abstract boolean isValidPossessor(SQLLine l, GameObject possessor);
	abstract boolean isValidElement(SQLLine l);
    abstract String getTable( );
    abstract GameObject default_instance();
    public int possessor() { return possessor ; }  
    public GameObject possessorObj() { return getById(possessor); }   
	abstract public String getFolder();  
	public String image() { return img; }  
	@Override public boolean equals(Object obj) { if(!(obj instanceof GameObject))return false;
	GameObject other = (GameObject) obj; if (id != other.id) return false; return true; }  
	//###############################################
	public static void reset() {
		for(  SparseArray<GameObject> a:mappe.values())
			a.clear();
		mappe.clear(); 
	}
}
