package me.nina.gameobjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import agame.SQLLine;
import agame.SQLTable;
import agame.Utils;
import agame.Utils.Point; 
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Zone.ZoneType; 



public class Zone extends GameObject {
	public static final int GRID_SIZE = 20;
    public final Map<Point, Set<Point>> grid = new HashMap<>(); 
	protected ZoneType type;
	protected Point p1,p2,p3 ;   
	//##############################################
	public static  enum ZoneSort {
		NORMAL(0),TPOINT(1),COLLISION(2),SPECIAL(3),STAIR(4),STAIRLIST(5),SPAWN_POINT(6) ; 
		private final int valeur; 
		ZoneSort(int valeur) { this.valeur = valeur;  } 
		public int getValeur() {  return valeur; } 
		public static ZoneSort from(String s) { 
			return from(Integer.parseInt(s)); }
		public static ZoneSort from(int valeur) {
			for (ZoneSort type : values()) 
				if (type.getValeur() == valeur) 
					return type;  
			throw new IllegalArgumentException("no zonesort for " + valeur);
		}
	}
	//##############################################
	public static enum ZoneType {
		RECTANGLE(0),ARC(-3),LINE(-1),  OVALE(1),UNFILLED_RECT(2),UNFILLED_OVALE(3); 
	    private final int valeur; 
	    ZoneType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; }  
	    public static ZoneType from(String s) {  return from(Integer.parseInt(s)); }
	    public static ZoneType from(int valeur) {
	        for (ZoneType type : values())   if (type.getValeur() == valeur)  return type;  
	        throw new IllegalArgumentException("no zonetype for " + valeur);
	    }
	} //##############################################
	public Zone(){}
	//##############################################
	public Zone(int x, int y, int i, int j, ZoneType t  ) { 
		super(); type=t;  p1 = new Point(x,y); p2 = new Point(i,j);  p3 = mid(); initGride( );
	} //##############################################
	public Zone(String line,int id) {  
		this(Integer.parseInt(line.split(",")[0]),Integer.parseInt(line.split(",")[1]),Integer.parseInt(line.split(",")[2]),Integer.parseInt(line.split(",")[3]),
				ZoneType.from(line.split(",")[4]),id );  
		try {p3=new Point( Integer.parseInt(line.split(",")[5]),Integer.parseInt(line.split(",")[6]));  grid.clear(); initGride( );  } catch (Exception e) { } //if curved line
	} //############################################## 
	public Zone(int x, int y, int i, int j, ZoneType t, int id) {
		super(SQLTable.getTable("zonelist").getById(id));
		type=t; p1 = new Point(x,y); p2 = new Point(i,j);  p3 = new Point(x()+w()/2, y()+h()/2);initGride( );
	} //############################################## 
	public Point mid() { int midX = (x() + i()) / 2; int midY = (y() + j()) / 2;
	    return new Point(midX, midY); } 
	//##############################################
	public void initGride( ) {  
		 
		List<Point> points = new ArrayList<>();  
	if(type == ZoneType.OVALE) 			for(Point p:Utils.getUnfilledOvale(x(),y(),i(),j())) points.add(p); 
	if(type == ZoneType.RECTANGLE) 		for(Point p:Utils.getUnfilledRectangle(p1, p2)) points.add(p);  
	if(type == ZoneType.LINE) 			for(Point p:Utils.getLinePoints(x(),y(),i(),j())) points.add(p);
	if(type == ZoneType.ARC) 			for(Point p:Utils.getArcPoints(x(),y(),i(),j(),p3.x,p3.y)) points.add(p); 
	if(type == ZoneType.UNFILLED_OVALE) for(Point p:Utils.getUnfilledOvale(x(),y(),i(),j())) points.add(p);
	if(type == ZoneType.UNFILLED_RECT) 	for(Point p:Utils.getUnfilledRectangle(p1,p2)) points.add(p);
	 


	  for (Point p : points) {
            Point gridPoint = getGridPointByNormalPoint(p);
             
            if (!grid.containsKey(gridPoint)) {
                grid.put(gridPoint, new HashSet<Point>());
            }
            grid.get(gridPoint).add(p);
             
        }
	  points.clear();
	 
} 
	 public static Point getGridPointByNormalPoint(Point p) {
	        int gridX = p.x / GRID_SIZE;
	        int gridY = p.y / GRID_SIZE;
	        return new Point(gridX, gridY);
	    }
	
	
	
	    public boolean isInZone(Point normal) { 
	    	if(this.type==ZoneType.RECTANGLE||(type==ZoneType.UNFILLED_RECT&&this instanceof SpawnPoint))return Utils.isInRectangle(normal, x(), y(), i(), j());
	    	if(this.type==ZoneType.OVALE||(type==ZoneType.UNFILLED_OVALE&&this instanceof SpawnPoint))return Utils.isInOval(normal, x(), y(), i(), j());
	        Point gridPoint = getGridPointByNormalPoint(normal);
	        Set<Point> boundingBoxPoints = grid.get(gridPoint); 
	        if(boundingBoxPoints==null)return false;
	        return  boundingBoxPoints.contains(normal);
	    }
	
	
	    public boolean isNear(Point p,  int tolerance) { 
	    	if(this.type==ZoneType.RECTANGLE)return Utils.isInRectangle(p, x(), y(), i(), j(),tolerance);
	    	if(this.type==ZoneType.OVALE)return Utils.isInOval(p, x(), y(), i(), j(),tolerance);
	    	Point gridPoint = Zone.getGridPointByNormalPoint(p);
	         
	        Set<Point> linePoints = grid.get(gridPoint); 
	        if (linePoints != null) {
	            for (Point linePoint : linePoints) {
	                if (Utils.getDistanceBetweenPoints(p, linePoint) <= tolerance) {
	                    return true;
	                }
	            }
	        }
	        return false;
	    }
	//##############################################  
	public ZoneType getType() { return type; }  
	public int x() {return p1.x;} public int y() {return p1.y;}
	public int i() {return p2.x;} public int j() {return p2.y;}
	public int w() {return Math.abs(p1.x - p2.x);} public int h() {return Math.abs(p1.y - p2.y);} 
	@Override public String getFolder() { return null; }
	public Point getIntersectPoint(Zone l) { 
	    for (Map.Entry<Point, Set<Point>> entry : grid.entrySet()) {
	        Point gridPoint = entry.getKey();
	        Set<Point> currentZonePoints = entry.getValue();
	        Set<Point> otherZonePoints = l.grid.get(gridPoint);
	        if (otherZonePoints != null) {
	           
	            for (Point p1 : currentZonePoints) 
	                for (Point p2 : otherZonePoints) 
	                    if (Utils.getDistanceBetweenPoints(p1, p2) < 6)
	                        return p2;
	        }
	    }
	    return null;
	}
	public boolean isCollided(Zone l) {  //used in stair colimaçon, need to be tested
		return Utils.isCollided(l.x(), l.y(), l.i(), l.j(),x(),y(),i(),j()); 
	} //############################################## 
	@Override GameObject instanciate(SQLLine l) {
		if (l.getInt("type") == ZoneSort.NORMAL.getValeur())
    		return new Zone(l.getString("value"),l.getId()); 
    	else if (l.getInt("type") == ZoneSort.TPOINT.getValeur())
    		return new TransitionPoint(l.getString("value"),l.getId());
    	else if (l.getInt("type") == ZoneSort.SPECIAL.getValeur())
    		return new SpecialZoneInv(l.getString("value"),l.getId());
    	else if (l.getInt("type") == ZoneSort.STAIR.getValeur())
    		return new Stair(l.getString("value"),l.getId());
    	else if (l.getInt("type") == ZoneSort.STAIRLIST.getValeur())
    		return new StairList(l.getString("value"),l.getId());
    	else if (l.getInt("type") == ZoneSort.SPAWN_POINT.getValeur())
    		return new SpawnPoint(l.getString("value"),l.getId());
		return null;
	} //######### 
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		 return l.getInt("type") == ZoneSort.NORMAL.getValeur() && l.getInt("possessor") == possessor.line.getId();
	} //########## 
	@Override boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.NORMAL.getValeur())
     		return true;
		return false;
	} //######### 
	@Override String getTable() {  return "zonelist"; } 
	@Override GameObject default_instance() {  return instance;  }  
	public static List<Zone> getByOwner(int ownerId){
		 return getbyPossessor(instance,GameObject.getById(ownerId)); }
	public static List<Zone> getAll( ) { return getAll(instance); }
	public static Zone get( int id) { return get(instance,id); }
	static GameObject instance =   new Zone(0, 0, 0, 0, ZoneType.ARC );
	//############################################## 
	@Override public boolean equals(Object obj) {
		if(!(obj instanceof Zone)) return false;
		if (this == obj) return true;  
		Zone other = (Zone) obj;
		if (p1 == null) { if (other.p1 != null) return false;
		} else if (!p1.equals(other.p1)) return false;
		if (p2 == null) { if (other.p2 != null) return false;
		} else if (!p2.equals(other.p2)) return false; return true;
	}


   
}
