package me.nina.gameobjects;
  
import java.util.List;
import java.util.Set;

import agame.SQLLine;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Character.PosType;

public class CharacterState extends GameObject {  

	private double scal =-1.0;
	private CharacterState(SQLLine l) { super(l); } 
	public CharacterState( ) { }
	//###############################################
	public static CharacterState get(int id) {   return get(instance,id); }
	public static List<CharacterState> getAll() { return getAll(instance); }  
	public static List<CharacterState> getByPossessor(int possessor) { return getbyPossessor(instance,GameObject.getById(possessor)); } 
	//###############################################
	public Animation movingAnimation(PosType type){   try { 
		return Animation.get( Integer.parseInt(
						 line .getString("moveanimation", type.getValeur()+"")
						 ));  } catch ( Exception e) {return null; }  
	} //###############################################
	public Animation idleAnimation(PosType type){   try {
		return Animation.get( Integer.parseInt(
						 line .getString("idleanimation", type.getValeur()+"")
						 )); } catch ( Exception e) {return null; }  
	}//###############################################
	public Animation talkAnimation(PosType type){  try { 
		 return Animation.get( Integer.parseInt(
						 line .getString("talkanimation", type.getValeur()+"")
						 )); } catch ( Exception e) {return null; }  
	}
	//###############################################
	@Override public String getFolder() { return possessorObj().getFolder()+title()+"/";  }
	 
	@Override GameObject instanciate(SQLLine l) {  return new CharacterState(l); } 
	@Override boolean isValidElement(SQLLine l) {  return true; }

	@Override String getTable() {  return "characterstate"; }
	static CharacterState instance= new CharacterState( );
	@Override GameObject default_instance() {  return instance; }

	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { return possessor.line.getId() == l.getInt("possessor") ; }
 
	public double scaleCorrection() {if (scal == -1.0)
		try { scal =  Double.parseDouble(line.getString("scalecorrection"));
		} catch (NumberFormatException e) {  scal = 0;  } 
		return scal;
	}
}
