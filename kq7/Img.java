package com.example.kq7;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
 
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Img {
	
	  String path;  public int shadow; public List<Bitmap> frames = new ArrayList< >();
	  public Img(String path) {
		  this(path, -1);
	  }
	  
	//##################################################################
	  public Img(String path, int shadow) {
	        this.path = path; 
	        this.shadow = shadow;         
	        try { 
	        	GifDecoder decoder = new GifDecoder();
	        	AssetManager assetManager = Main.mContext.getAssets();
	        	InputStream stream = assetManager.open(path);
	        	decoder.read(stream);
	        	decoder.complete(); 
	        	for(Bitmap fram : decoder.frames)
	        		frames.add(ImgUtils.getCopy(fram)); 
	        	stream.close();
	        	if (frames.isEmpty()) frames.add(getPicture( )); 
	        	
	        } catch (Exception e) {Main.log(e ); }
	       if(shadow !=-1)
	    	   for(Bitmap fram:frames)
	    		   ImgUtils.applyShadow(fram, shadow);
	    		   
	    }
	//##################################################################
	private Bitmap getPicture() throws IOException {
		 AssetManager assetManager = Main.mContext.getAssets();
         InputStream inputStream = assetManager.open(path);
         Bitmap loadedImage = BitmapFactory.decodeStream(inputStream);
         inputStream.close();
         return loadedImage;
          
	}
	public int getWidth() { 
		return frames.get(0).getWidth();
	}
	public int getHeight() { 
		return frames.get(0).getHeight();
	}
	

	    
}
