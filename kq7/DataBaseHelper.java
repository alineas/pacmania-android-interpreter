package com.example.kq7;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

public class DataBaseHelper extends SQLiteOpenHelper { 
    private static String DB_NAME = "game.npcma";
    public static SQLiteDatabase myDataBase;
    private String DB_PATH;
    private Context context;
	public static boolean loaded = false;

    public DataBaseHelper(Context context)  {
        super(context, DB_NAME, null, 1);
        this.context = context;
        DB_PATH = context.getApplicationContext().getDatabasePath(DB_NAME).getPath();
        if (Build.VERSION.SDK_INT < 23) {
        	DB_PATH = context.getApplicationContext().getDatabasePath(DB_NAME).getParent() ;
        	new File(context.getApplicationContext().getDatabasePath(DB_NAME).getParent()).mkdirs();
        }
        initializeDatabase();
    }

    private void initializeDatabase() {
        boolean dbExist = checkDatabase();
        if (!dbExist) {
            File dbDir = new File(DB_PATH).getParentFile();
            if (!dbDir.exists()) {
                dbDir.mkdirs();
            }
            createDatabase();
        } else {
             
            boolean isReinstalling = true;
            
            
            
            
            
            
            if (isReinstalling) {
                // Supprimer la base de données existante
                deleteDatabase();
                // Charger la nouvelle base de données
                createDatabase();
                
            } else {
                openDatabase();
            }
        }
    }

    private boolean checkDatabase() {
        SQLiteDatabase checkDB = null;
        if (Build.VERSION.SDK_INT > 23) 
        try {
            String myPath = DB_PATH;
            if (Build.VERSION.SDK_INT < 23) 
            	checkDB = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            // La base de données n'existe pas encore
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    private void createDatabase() {
        try {
            copyDatabase();
            openDatabase();
        } catch (IOException e) {
            Main.log(e);
        }
    }

    private void copyDatabase() throws IOException {
        InputStream myInput = context.getAssets().open(DB_NAME);
        OutputStream myOutput = new FileOutputStream(DB_PATH);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
        File dbFile = new File(DB_PATH);
        Main.log("_________________"+dbFile.exists());
    }

    private void openDatabase() {
    	 if (Build.VERSION.SDK_INT < 23) 
    	myDataBase = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
    	 else myDataBase = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY);
        myDataBase.disableWriteAheadLogging();
        loaded =true;
    }

    private void deleteDatabase() {
        File dbFile = new File(DB_PATH);
        if (dbFile.exists()) {
            dbFile.delete();
        }
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null) {
            myDataBase.close();
        }
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) { }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }
}