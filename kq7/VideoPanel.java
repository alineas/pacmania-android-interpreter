package com.example.kq7;

import agame.Utils.Point;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class VideoPanel extends RelativeLayout {  
	
	private float initialDistance = -1; 
	private static final long LONG_PRESS_DURATION = 1500; // Durée en millisecondes pour considérer un appui long
	private boolean isLongPress = false;
	private long touchStartTime;  
	public VideoPanel(Context context) { super(context);  init(context);  } 
    public VideoPanel(Context context, AttributeSet attrs) { super(context, attrs);   init(context); } 
    public VideoPanel(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle);  init(context); }

    private void init(Context context) {try { 
       //maybe one day...
    } catch (Exception e) { Main.log(e); }}

    @Override protected void onDraw(Canvas g) {
        super.onDraw(g); 
        if(Main.instance != null)
        	Main.instance.paint(g);//draw the game in the Main
       
    }
  //###############################################
  //###############################################
  //############################################### mouse handler
  //###############################################
    @Override public boolean performClick() { 
        return super.performClick();
    }
    boolean wasFullscreenClick;
    @Override public boolean onTouchEvent(MotionEvent event) {
    	
    	requestDisallowInterceptTouchEvent(true);//stop scrollbars to act
    	int[] viewLocation = new int[2];//convert clic pos to the screenlocation 
		Main.menuBar.getLocationOnScreen(viewLocation);   
		int menuX = (int) ((event.getRawX() - viewLocation[0])/Main.zoom); 
		int menuY = (int) ((event.getRawY() - viewLocation[1])/Main.zoom);
		MenuBar.inHandX = (int) (event.getRawX() - viewLocation[0] ); 
        MenuBar.inHandY = (int) (event.getRawY() - viewLocation[1] );
        switch (event.getAction()) {
        	//###############################################
            case MotionEvent.ACTION_DOWN:
            	touchStartTime = System.currentTimeMillis();
                isLongPress = false;
                 
        		
        		if (Main.instance != null&& Main.instance.objMgr.isFullScreen()) {//in fs we can take an object like in temple
        			Point p = new Point((int) ((int) event.getX()/Main.zoom),(int) ((int) event.getY()/Main.zoom)); 
        			Main.menuBar.hasObject(menuX,menuY);//pick inv obj in fs
        			if( Main.instance.objMgr.objectHasBeenClicked(Main.instance.player,p) ) {
        				wasFullscreenClick = true;
        				return true;
        			}
        		}
                if(Main.menuBar.hasObject(menuX,menuY)) return true; //inv objet has been picked
            //###############################################   
            case MotionEvent.ACTION_MOVE: //inv object already picked : move it
            	if(Main.menuBar.inHand!=null||Main.menuBar.inHandObj!=null) { 
            		MenuBar.inHandX = (int) (event.getRawX() - viewLocation[0] ); 
            		MenuBar.inHandY = (int) (event.getRawY() - viewLocation[1] ); 
            		return true;
            	}
            	//############## zoom
                if (event.getPointerCount() == 2) { //else : we maybe doing a zoom
                	touchStartTime = System.currentTimeMillis();// its not long press
                    float x1 = event.getX(0); 	float y1 = event.getY(0);
                    float x2 = event.getX(1); 	float y2 = event.getY(1);
                    float newDistance = distanceBetweenPoints(x1, y1, x2, y2);

                    if (initialDistance == -1)  initialDistance = newDistance;//start to pinch
                    else {//do the zoom action
                    	float currentZoom = newDistance / initialDistance; 
                        currentZoom = (float) Math.max(0.1, Math.min(10, currentZoom));//zoom from 0 to 10 
                        setZoom(currentZoom);
                        return true;
                    }
                }
              //############## if locked in a collision
                long pressDuration = System.currentTimeMillis() - touchStartTime;
                if (pressDuration >= LONG_PRESS_DURATION) {
                    isLongPress = false; 
                    if (Main.instance != null&&!Main.instance.menuBar.freezed&&!Main.instance.objMgr.isFullScreen()) {
                        int x = (int) event.getX(0);
                        int y = (int) event.getY(0);
                       // Main.instance.player.setPos(new Point((int) (x / Main.zoom), (int) (y / Main.zoom)));
                    }
                    touchStartTime = System.currentTimeMillis();
                    return true;
                }
                break;
            //###############################################
            case MotionEvent.ACTION_UP: //release : act has we clicked
            	performClick();
                initialDistance = -1;  //cancel zoom value 
                
                if ( Main.instance != null) {int x = (int) event.getX(0); 	int y = (int) event.getY(0);
                    
                	if (isLongPress)  //if locked in a collision
                		Main.instance.player.setPos(new Point((int) (x/Main.zoom),(int) (y/Main.zoom)));
                	else if( !wasFullscreenClick&&!Main.instance.objMgr.isFullScreen()||Main.menuBar.inHandObj!=null||Main.menuBar.inHand !=null){ 
                		if ( !Main.menuBar.clic(menuX,menuY))//already handled in down action for fs, but allow to put hand obj down
                			Main.instance.clic(event);
                	}
                }
                wasFullscreenClick=false;  
                 
                
                break;
        }
        
        long pressDuration = System.currentTimeMillis() - touchStartTime;
        if (pressDuration >= LONG_PRESS_DURATION) { 
            isLongPress = true; 
        }
        
        return true; // Indique que l'événement a été consommé
    }
  //###############################################
  //###############################################
    public static void calculateAndSetInitialZoom() {
        DisplayMetrics displayMetrics = Main.mContext.getResources().getDisplayMetrics();
        int screenHeight = displayMetrics.heightPixels; // Utiliser la hauteur de l'écran
        int initialImageHeight = Main.instance.actualSceneOnScreen.bgImage().getHeight(); // Utiliser la hauteur de l'image
        float initialZoom = (float) screenHeight / initialImageHeight; // Calculer le zoom pour remplir la hauteur
        setZoom(initialZoom);
    }
  //###############################################
  //###############################################
	public static void setZoom(float z) {
		 Main.zoom = z;
		 MenuBar.igMenu.setZoom(z);
		 MenuBar.invExplorer.setZoom(z);
         Main.menuBar.setZoom(z);
         
		 Main.instance.backgroundImage=ImgUtils.scal(Main.instance.actualSceneOnScreen.bgImage(),z);
			VideoPanel monLayout = (VideoPanel) Main.mContext.findViewById(R.id.videopanel); 
	        ViewGroup.LayoutParams layoutParams = monLayout.getLayoutParams();
	        
	        layoutParams.width = Main.instance.backgroundImage.getWidth();
	        layoutParams.height = Main.instance.backgroundImage.getHeight(); 
	            monLayout.setLayoutParams(layoutParams); 
	             
	}
	//###############################################
    private float distanceBetweenPoints(float x1, float y1, float x2, float y2) {
        float dx = x2 - x1;
        float dy = y2 - y1;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }
}
 