package com.example.kq7;

import agame.Ashe;
import agame.Ashe.AshType;
import agame.Utils.Point;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;

public class InvExplorer extends View {//TODO:need to center this
	 
	 int movableX=-1,movableY;  Paint paint ;
	//######################################
	public InvExplorer(Context context) {  super(context); init(context); } 
	public InvExplorer(Context context, AttributeSet attrs) { super(context, attrs);  init(context); } 
	public InvExplorer(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle); init(context); }
    public void init(Context context ) {try { 
        if(Main.mContext == null )Main.mContext = (MainActivity) context;
        MenuBar.invExplorer = this;
        setVisibility(View.INVISIBLE);
        paint = new Paint();
 		LayoutParams layoutParams = new LayoutParams(invZoneTitles().w(), invZoneTitles().h());
        layoutParams.leftMargin = invZoneTitles().x(); 
        layoutParams.topMargin = invZoneTitles().y();  
        
        setOnTouchListener(new View.OnTouchListener() { @Override public boolean onTouch(View v, MotionEvent event) {try {
		    if(getVisibility()==View.INVISIBLE)return false;    
        	int x = (int) ((int) event.getX()/Main.zoom); int y = (int) ((int) event.getY()/Main.zoom); 
		        if (event.getAction() == MotionEvent.ACTION_DOWN) {Main.log("down");
		            closInvExplorerIfNeeded(x, y);
		            handleZoneClickInvExplorer(x, y);
		        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {Main.log("move");
		            moveObjectInExplorer(x, y);
		        }
		    } catch (Exception e) { Main.log(e); }  return true; } });
    } catch (Exception e) { Main.log(e); } }
 
  //######################################
    @Override protected void onDraw(Canvas canvas) {try {
        super.onDraw(canvas);
        Bitmap bg = ImgUtils.getPicture(invExplorerBg());
        bg = ImgUtils.scal(bg, Main.zoom);
        canvas.drawBitmap(bg, 0,0,null); 
        
        Bitmap button = ImgUtils.getPicture(exitButtonInvExplorerBg());
        button = ImgUtils.scal(button, Main.zoom);
        canvas.drawBitmap(button, closeInvExplorerZone().x()*Main.zoom,closeInvExplorerZone().y()*Main.zoom,null); 
        
        if(Main.menuBar.inHand != null&&Main.menuBar.inHandAnim != null) {
        	Bitmap img = Main.menuBar.inHandAnim.getActualFrame();
        	img = ImgUtils.scal(img, Main.zoom);
        	int x=bg.getWidth()/2-img.getWidth()/2;
			int y= bg.getHeight()/2-img.getHeight()/2;
        	canvas.drawBitmap(img, x,y,null);
        	 
           
            paint.setColor(Color.WHITE);
            paint.setTextSize(15 * getResources().getDisplayMetrics().density*Main.zoom); // Set text size
            paint.setTextAlign(Paint.Align.CENTER);
            
            String txt = Main.menuBar.inHand.title();
            if(Main.menuBar.inHand.selectedState.translateId()!=-998)
            	txt = Message.get(Main.menuBar.inHand.selectedState.translateId()).line.getString("text");
            canvas.drawText(txt,  (int) (invZoneTitles().mid().x*Main.zoom),(int) (invZoneTitles().mid().y*Main.zoom), paint);  
        }
    } catch (Exception e) { Main.log(e); }}
     
	//######################################
	private void moveObjectInExplorer(int newX, int newY) { 
 	   if( Math.abs(movableX - newX) <10 && Math.abs(movableY - newY) <10 )return; 
 	    if (newX > movableX && newY > movableY)  Main.menuBar.inHandAnim.rewindOneFrame();  
 	    else if (newX < movableX && newY > movableY) Main.menuBar.inHandAnim.forwardOneFrame();  
 	    else if (newX > movableX && newY < movableY)  Main.menuBar.inHandAnim.rewindOneFrame();  
 	    else if (newX < movableX && newY < movableY) Main.menuBar.inHandAnim.forwardOneFrame();  
 	    movableX = newX;
 	    movableY = newY;
	}
	 
	//######################################
	private void closInvExplorerIfNeeded(int x, int y) {
			if(closeInvExplorerZone().isInZone(new Point(x,y))) {
				Main.menuBar.freezed=false;
				setVisibility(View.INVISIBLE); 
				Main.instance.removeAsh(new Ashe(Main.menuBar.inHand.line.getId(),AshType.HAS_IN_HAND,1));
	        	Main.menuBar.inHandAnim=null;
	        	Main.menuBar.inHand =null;
	        } 
	} 
	//###################################################  
	private void handleZoneClickInvExplorer(int unzoomedX, int unzoomedY) {try {
			 if(Main.menuBar.inHand != null && Main.menuBar.inHandAnim != null)
				for(Zone z: SpecialZoneInv.getByOwner(Main.menuBar.inHand.getSelectedState() .line.getId())) {//tcheck inv zones
					
					Bitmap bg = ImgUtils.getPicture(invExplorerBg()); 
					Bitmap handImage = Main.menuBar.inHandAnim.getActualFrame(); 
					int objectWidth = handImage.getWidth();
					int objectHeight = handImage.getHeight(); 
					int imageX = (bg.getWidth() - objectWidth) / 2;
					int imageY = (bg.getHeight() - objectHeight) / 2; 
					int relativeX = unzoomedX - imageX;
					int relativeY = unzoomedY - imageY;

				    Point p = new Point(relativeX, relativeY);//get the relative clic

		                Main.log(relativeX+" "+relativeY);
					 if(z.isInZone(p) 
							 && (((SpecialZoneInv)z).frames.contains(-1) || ((SpecialZoneInv)z).frames.contains(Main.menuBar.inHandAnim.currentIndex()))) {
						Main.instance.actionMgr.handleAction( Action.getByDetector(z ),Main.instance.player);
						Main.menuBar.inHandAnim=Animation.get(Main.menuBar.inHand.getSelectedState().animID());//if changed state
						Main.menuBar.inHandAnim.forwardAll(); 
					}  
				 }  
	} catch (Exception e) { Main.log(e); }  }
	
	public Zone invZoneTitles() { return Zone.get(MenuBar.getGameInfo().getInt("menubar","invzonetitles"));}
	public String invExplorerBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "invbgname") ;}
	public String exitButtonInvExplorerBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "exitexplorerbg") ;}
	public Zone closeInvExplorerZone() { return Zone.get(MenuBar.getGameInfo().getInt("menubar","closeinvexplorerzone"));}
	public void setZoom(float z) {
		// TODO Auto-generated method stub
		
	}
	
}