package com.example.kq7;
  
import agame.SoundPlayer;
import agame.Utils.Point; 
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap; 
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View; 
import android.widget.Button;
import android.widget.FrameLayout; 
import android.widget.ImageView; 
import android.widget.SeekBar;
import android.widget.TextView;
import me.nina.gameobjects.Zone;

public class IgMenu extends FrameLayout{//TODO : all the menu
	private SeekBar seekBarFX; 	private SeekBar seekBarMusic; 	private Button restoreButton;
	private ImageView exitButton;
	private TextView musicVolumeLabel;
	private TextView fxVolumeLabel;
	private float zoom =1f; 
	//###################################################  
	public IgMenu(Context context) {  super(context);  init(context);  } 
    public IgMenu(Context context, AttributeSet attrs) { super(context, attrs); init(context); } 
    public IgMenu(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle); init(context); } 
    public void init(Context context )  {try { 
    	if(Main.mContext == null )Main.mContext = (MainActivity) context;
    	MenuBar.igMenu = this;
    	
    	Bitmap bg = ImgUtils.getPicture(igMenuBg());  
    	BitmapDrawable backgroundDrawable = new BitmapDrawable(getResources(), bg); 
        backgroundDrawable.setGravity(Gravity.TOP | Gravity.START);//TODO:need to center this
        setBackground(backgroundDrawable);
    	
        musicVolumeLabel = new TextView(context); musicVolumeLabel.setText("Music Volume");musicVolumeLabel.setBackgroundColor(Color.WHITE);
        fxVolumeLabel = new TextView(context);    fxVolumeLabel.setText("FX Volume");fxVolumeLabel.setBackgroundColor(Color.WHITE);
        
        seekBarFX = new SeekBar(getContext()); seekBarFX.setMax(100); seekBarFX .setProgress(80);
        seekBarFX.getProgressDrawable().setTintList(ColorStateList.valueOf(Color.RED)); seekBarFX.setBackgroundColor(Color.WHITE);
       
        seekBarMusic = new SeekBar(getContext()); seekBarMusic.setMax(100);seekBarMusic .setProgress(60);
        seekBarMusic.getProgressDrawable().setTintList(ColorStateList.valueOf(Color.RED)); seekBarMusic.setBackgroundColor(Color.WHITE);
        
        restoreButton = new Button(getContext()); restoreButton.setText("Restore Game");restoreButton.setBackgroundColor(Color.WHITE);
        
        Bitmap bmp = ImgUtils.getPicture(exitButtonMenuBg());
        exitButton = new ImageView(context); exitButton.setImageBitmap(bmp);
        
        setBounds(musicVolumeLabel,10,100,    100,20,true);//TODO: need to add these zones in the igmenu editor
        setBounds(seekBarMusic,   120,100,    100,20,true);
        setBounds(fxVolumeLabel,   10,120,    100,20,true);
        setBounds(seekBarFX,      120,120,    100,20,true);
        setBounds(restoreButton,  10,140,    100,40,true);
        setBounds(exitButton,     closeMenuZone().x(),closeMenuZone().y(),    closeMenuZone().w(),closeMenuZone().h(),true);
 
        	 
        seekBarFX.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            	 
            	//float value = (float) seekBar.getProgress() / seekBar.getMax();
            	 SoundPlayer.fxVolume = seekBar.getProgress(); 
            } 
            @Override public void onStartTrackingTouch(SeekBar seekBar) {} 
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        }); 
        seekBarMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            	 
            	Main.instance.soundPlayer.adjustMidiVolume((int) seekBar.getProgress());
            	 
            	} 
            @Override public void onStartTrackingTouch(SeekBar seekBar) {} 
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        }); 
        restoreButton.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
            	Main.instance.restoreGame();
            }
        });
       
        goVisibility(false);
        
    } catch (Exception e) { Main.log(e); } }
 
  private void setBounds( View v, int i, int j, int width, int height,boolean add) {
	  FrameLayout.LayoutParams params = new FrameLayout.LayoutParams( width, height );
  	params.gravity = Gravity.TOP | Gravity.START;  
  	params.leftMargin = i;
  	params.topMargin = j;  
  	v.setLayoutParams(params);
  	if(add)addView(v);
	}
//###################################################  
  @Override public boolean performClick() { 
      return super.performClick();
  }
    @Override public boolean onTouchEvent(MotionEvent e) { performClick();try { 
    	if(closeMenuZone().isInZone(new Point((int)( e.getX()/zoom),(int) (e.getY()/zoom)))) {
    		Main.menuBar.freezed=false;
    		goVisibility(false);
    		return true;
    }} catch (Exception ee) { Main.log(ee); } return false;}
    
  //###################################################  
    public void goVisibility(boolean visible) {
    	int childCount = getChildCount();
    	for (int i = 0; i < childCount; i++) {
    		View child = getChildAt(i);
    		if(!visible) {
    			child.setVisibility(View.INVISIBLE);
    			setVisibility(View.INVISIBLE);
    			invalidate();
    		}
    		else { 
    			setVisibility(View.VISIBLE);
    			child.setVisibility(View.VISIBLE);
    			invalidate();
    		}
    	}
    }
    
    
    public Zone closeMenuZone() { return Zone.get(MenuBar.getGameInfo().getInt("menubar","closemenuzone"));}
    public String igMenuBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "igmenubg") ;}
	public String exitButtonMenuBg(){return "games/"+Main .gameName+"/"+MenuBar.getGameInfo().getString("menubar", "exitmenubg") ;}
	
	public void setZoom(float zoom) {this.zoom=zoom;
	    // Redimensionner l'image de fond avec le zoom spécifié
	    Bitmap bg = ImgUtils.getPicture(igMenuBg()); 
	    int newWidth = (int) (bg.getWidth() * zoom);
	    int newHeight = (int) (bg.getHeight() * zoom);
	    bg = ImgUtils.resiz(bg, newWidth, newHeight);

	    // Créer un Drawable avec l'image de fond redimensionnée
	    BitmapDrawable backgroundDrawable = new BitmapDrawable(getResources(), bg);
	    backgroundDrawable.setGravity(Gravity.TOP | Gravity.START);
	    setBackground(backgroundDrawable);

	    // Redimensionner et repositionner les autres éléments
	    setBounds(musicVolumeLabel, (int) (10 * zoom), (int) (100 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(seekBarMusic, (int) (120 * zoom), (int) (100 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(fxVolumeLabel, (int) (10 * zoom), (int) (120 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(seekBarFX, (int) (120 * zoom), (int) (120 * zoom), (int) (100 * zoom), (int) (20 * zoom),false);
	    setBounds(restoreButton, (int) (10 * zoom), (int) (140 * zoom), (int) (100 * zoom), (int) (40 * zoom),false);
	    setBounds(exitButton, (int) (closeMenuZone().x() * zoom), (int) (closeMenuZone().y() * zoom), 
	               (int) (closeMenuZone().w() * zoom), (int) (closeMenuZone().h() * zoom),false);
	}
}
