package com.example.kq7;

import java.io.OutputStream;
import java.io.PrintStream;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Zone;

public class MainActivity extends Activity { 
    private SurfaceView videoPanel; 	private MediaPlayer mediaPlayer;  
    private FrameLayout frameLayout;    private Main main;        
	private MainActivity context;
	private ProgressBar loadingProgress;
	private TextView textView;       public static DataBaseHelper sql; 
	 
	//############################################################################ start program
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.setErr(new LogPrintStream("kq7", System.err));
        Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler("kq7"));
        context = this;
        main = new Main(context);
        try {//setfullscreen
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN); 
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            //play a nice intro 
            frameLayout = new FrameLayout(this);
            setContentView(frameLayout);
            videoPanel = new SurfaceView(this);
            frameLayout.addView(videoPanel);
            
            //progressbar
            LinearLayout linearLayout = new LinearLayout(this); linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            FrameLayout.LayoutParams linearParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            linearParams.gravity = Gravity.BOTTOM; linearLayout.setLayoutParams(linearParams); frameLayout.addView(linearLayout);
            loadingProgress = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
            LinearLayout.LayoutParams progressParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
            loadingProgress.setLayoutParams(progressParams); linearLayout.addView(loadingProgress);
            loadingProgress.setMax(7); loadingProgress.setVisibility(View.VISIBLE); textView = new TextView(this);
            textView.setText("Loading..."); LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textView.setBackgroundColor(Color.BLACK);   textView.setTextColor(Color.WHITE); textView.setLayoutParams(textParams);
            linearLayout.addView(textView); textView.setVisibility(View.VISIBLE);
            
            //main panel
            videoPanel.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { } 
                @Override public void surfaceDestroyed(SurfaceHolder holder) { /*mediaPlayer.release();*/ }
                @Override public void surfaceCreated(SurfaceHolder holder) {try {
                    	
                //play intro
                	mediaPlayer = new MediaPlayer(); AssetFileDescriptor videoDescriptor = getAssets().openFd("intro.mp4");
                	mediaPlayer.setDataSource(videoDescriptor.getFileDescriptor(), videoDescriptor.getStartOffset(), videoDescriptor.getLength());
                	mediaPlayer.setDisplay(holder); mediaPlayer.prepare();
                	mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {@Override public void onCompletion(MediaPlayer mp) { 

                //launch game if playback finish	
                		launch();  } });
                	mediaPlayer.start();
                	
                	//during playback, start to load some things...
                	new Thread(new Runnable() {  @Override public void run() {  try {
                		sql = new DataBaseHelper(context);  updateProgress(0,"database");
                		while(!sql.loaded)Thread.sleep(100);//wait db to construct
                		sql.loaded = false;//wait again for the rest
                		main.init(); updateProgress(1,"animations");
                		Animation.getAll();updateProgress(2,"characters");
                		CharacterState.getAll(); updateProgress(3,"objects");
						Zone.getAll(); 
						ObjectState.getAll();updateProgress(4,"messages");
						Message.getAlls();updateProgress(6,"actions");
						Action.getAll();updateProgress(7,"done");
						sql.loaded=true;
						
						Main.log("parralel loading finiched"); 
                	} catch (Exception e) { Main.log(e); }   } }).start();
                } catch (Exception e) { Main.log(e); } }  });
        } catch (Exception e) {  Main.log(e); }
    }
  //############################################################################clic
    //fade , stop playback , launch game
    @Override public boolean onTouchEvent(MotionEvent event) {
    	if(!sql.loaded||mediaPlayer==null )return false;
    	float x = event.getX(); float y = event.getY();
          
    	 if(!clicOnBottomMenubar(x,y) && event.getAction() == MotionEvent.ACTION_DOWN)try{ 
    		videoPanel.invalidate();
        	final View blackOverlay = new View(this);
            blackOverlay.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            blackOverlay.setBackgroundColor(Color.BLACK);
            blackOverlay.setAlpha(0.0f); // Initially fully transparent
            frameLayout.addView(blackOverlay);
             
            new Thread(new Runnable() { @Override public void run() {
                	float volume = 1.0f; 
                    while (volume > 0) {
                        try { Thread.sleep(100); } catch (InterruptedException e) {Main.log(e);}
                        volume -= 0.02f; if (volume <= 0)  volume = 0; 
                        mediaPlayer.setVolume(volume, volume); 
                        blackOverlay.setAlpha(1-volume) ;  
                    }  
                   launch(); 
            }
            
		  }).start();  } catch (Exception e) {  Main.log(e); }else mediaPlayer.pause();
        return true;
    }
  //############################################################################
    private boolean clicOnBottomMenubar(float x, float y) {
    	Rect videoBounds = new Rect();
	    videoPanel.getHitRect(videoBounds); 
	    DisplayMetrics displayMetrics = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
	    int navigationBarHeight = 0;
	    int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
	    if (resourceId > 0)  navigationBarHeight = getResources().getDimensionPixelSize(resourceId); 
	    videoBounds.bottom -= navigationBarHeight; 
	    return !videoBounds.contains((int) x, (int) y);
	}
  //############################################################################
	private void launch() { runOnUiThread(new Runnable() { @Override public void run() {try{
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer=null;
			System.gc(); 
			context.setContentView(R.layout.activity_main); 
        	 main.start();
    } catch (Exception e) {  Main.log(e); }}  }) ;}
	//############################################################################

	
	
	private void updateProgress(final int i, final String text) {
		 runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	 loadingProgress.setProgress(i );
	                textView.setText(text);
	                if(i==7) {
	                	loadingProgress.setVisibility(View.GONE);
	                	textView.setText("clic on video for skip");
	                }
	            }
	        });
	}
	@Override public boolean onCreateOptionsMenu(Menu menu) {  return true; } 
	@Override public boolean onOptionsItemSelected(MenuItem item) {  return super.onOptionsItemSelected(item); }
	 
	@Override protected void onStart() {//TODO
        super.onStart();
        Log.d("MyTag", "Activity onStart");
    }

    @Override protected void onResume() {//TODO
        super.onResume();
        Log.d("MyTag", "Activity onResume");
    }

    @Override protected void onPause() {//TODO
        super.onPause();
        Log.d("MyTag", "Activity onPause");
    }

    @Override protected void onStop() {//TODO
        super.onStop();
        Log.d("MyTag", "Activity onStop");
    }

    @Override protected void onDestroy() {//TODO
        super.onDestroy();
        Log.d("MyTag", "Activity onDestroy");

        try {//Main.instance.stopLoop();
        } catch (Exception e) { Main.log(e); }
    }

     
	
   
}
class LogPrintStream extends PrintStream {
    private String tag;

    public LogPrintStream(String tag, OutputStream out) {
        super(out);
        this.tag = tag;
    }

    @Override
    public void println(String message) {
        Log.d(tag, message);
    }

    @Override
    public void print(String message) {
        Log.d(tag, message);
    }
}
class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    private String tag;

    public MyUncaughtExceptionHandler(String tag) {
        this.tag = tag;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Log.d(tag, "Uncaught exception in thread " + t.getName(), e);
        // Optionally, you can add code to restart the application or show a custom error screen
    }
}