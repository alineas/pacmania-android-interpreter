package com.example.kq7;
 
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix; 

public class ImgUtils {
 
    public static Map<String, Img> mappe = new HashMap<>();
    private static final int SHADOW_ALPHA = Color.argb(120, 0, 0, 0); // ombre (0-255)
  
    public static Bitmap getPicture(String path ) {
        return getPicture(path,0, -1);  
    }
 

    public static Bitmap getPicture(String path, int frame, int shadow) {
    	path=path.replaceAll("é|è", "e");
        if (!mappe.containsKey(path)) 
           mappe.put(path, new Img(path,shadow)); 
        return mappe.get(path).frames.get(frame);
    }
	public static Img getImg(String path, int shadow) {
		path=path.replaceAll("é|è", "e");
		if (!mappe.containsKey(path)) 
	           mappe.put(path, new Img(path,shadow)); 
		return mappe.get(path);
	}  
	//##################################################################
    public static Bitmap getCopy(Bitmap original) { 
        int width = original.getWidth();
        int height = original.getHeight(); 
        Bitmap copiedBitmap = Bitmap.createBitmap(width, height, original.getConfig()); 
        Canvas canvas = new Canvas(copiedBitmap); 
        canvas.drawBitmap(original, 0, 0, null);
 
        return copiedBitmap;
    }
  //##################################################################
    public static void applyShadow(Bitmap originalImage, int shadow) {
        int imageWidth = originalImage.getWidth();
        int imageHeight = originalImage.getHeight();
        int[] pixels = new int[imageWidth * imageHeight];
       try { originalImage.getPixels(pixels, 0, imageWidth, 0, 0, imageWidth, imageHeight);

        
			for (int i = 0; i < pixels.length; i++) {
			    if (pixels[i] == shadow) {
			        pixels[i] = SHADOW_ALPHA;
			    }
			}
		

        originalImage.setPixels(pixels, 0, imageWidth, 0, 0, imageWidth, imageHeight);} catch (Exception e) {
			Main.log(e);
		}
    }
  //##################################################################
	public static Bitmap scal(Bitmap original, double scale) {
		int w = original.getWidth();
        int h = original.getHeight(); 
        Matrix matrix = new Matrix();
        matrix.postScale((float) scale, (float) scale); 
        return Bitmap.createBitmap(original, 0, 0, w, h, matrix, true); 
        
	}
	//##################################################################
	public static Bitmap resiz(Bitmap image, int width, int height) {
		 int currentWidth = image.getWidth();
	        int currentHeight = image.getHeight(); 
	        float scaleWidth = (float) width / currentWidth;
	        float scaleHeight = (float) height / currentHeight;
	        float scale = Math.min(scaleWidth, scaleHeight); 
	        int newWidth = Math.round(currentWidth * scale);
	        int newHeight = Math.round(currentHeight * scale); 
	        return Bitmap.createScaledBitmap(image, newWidth, newHeight, true);
 
	}
    
     
    
 

}
