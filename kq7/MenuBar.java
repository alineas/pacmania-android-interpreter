package com.example.kq7;
   
import java.util.ArrayList;
import java.util.List;
import java.util.Set; 
import agame.Ashe;
import agame.SQLLine;
import agame.SQLTable;
import agame.Utils;
import agame.Ashe.AshType;
import agame.Utils.Point;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet; 
import android.view.View;
import android.widget.FrameLayout; 
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation; 
import me.nina.gameobjects.Zone; 
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.Zone.ZoneType; 

public class MenuBar extends FrameLayout { 
	
    public List<InventoryObject> inventory = new ArrayList<>(); 
    public boolean freezed;
    public InventoryObject inHand;
    public Animation inHandAnim; 
	public BasicObject inHandObj; //ground objects we can take 
	Bitmap inHandBmp;
	private int menuX,menuY,menuWidth,menuHeight; 
	public static InvExplorer invExplorer;
	public static IgMenu igMenu;
	public static int inHandY,inHandX; 
    float zoom = 1f;
	private Bitmap bg;
	
	//############################################################################
	public MenuBar(Context context) { super(context); init(context);   } 
	public MenuBar(Context context, AttributeSet attrs) { super(context, attrs);  init(context);  } 
	public MenuBar(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle); init(context); } 
	
	//setbounds
	private void init(Context context) { try { Main.log("a");
		if(Main.mContext == null )Main.mContext = (MainActivity) context;Main.log("b");
		   bg=ImgUtils.getPicture(bgFolder());Main.log("c");
		menuX = menuZone().x();  menuY = menuZone().y(); Main.log("d");
		menuWidth = menuZone().w();  menuHeight = menuZone().h();   Main.log("e");
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(menuWidth, menuHeight);Main.log("f");
		layoutParams.leftMargin = menuX ;   layoutParams.topMargin = menuY;   Main.log("g");
		setLayoutParams(layoutParams); 
	} catch (Exception e) { Main.log(e); }}
		 
	//############################################################################ paint
    @Override protected void onDraw(Canvas canvas) { try { 
    	if(bg==null)bg =ImgUtils.scal( ImgUtils.getPicture(bgFolder()),zoom);
        canvas.drawBitmap(bg,menuX*zoom,menuY*zoom,null); 
        populateInventory();
        for (InventoryObject obj : inventory) {
            Animation animation = Animation.get(obj.getSelectedState().animID());
            Bitmap frame = animation.getActualFrame();
            frame=ImgUtils. resiz(frame,(int) (30 * zoom), (int) (30 * zoom)); 
            canvas.drawBitmap(frame, obj.zone.x(), obj.zone.y(), null);
        }
        if(invExplorer!=null&&invExplorer.getVisibility()==View.VISIBLE)
        	invExplorer.invalidate(); //draw the turning inv object
        if(inHandAnim !=null &&inHand !=null && invExplorer.getVisibility()==View.INVISIBLE) { //invobj
        	Bitmap img = inHandAnim.getActualFrame();
        	img = ImgUtils. resiz (img,(int) (30 * zoom), (int) (30 * zoom)); 
        	canvas.drawBitmap(img,inHandX-img.getWidth()/2,inHandY-img.getHeight()/2-70,null); //70 is a finger size ?
        }
        if(inHandObj!=null) {//groundobj
        	if(inHandBmp ==null)  
        	inHandBmp = Animation.get(inHandObj.getSelectedState().animID()).getLastFrame(); 
        	  
        	canvas.drawBitmap(inHandBmp,inHandX-inHandBmp.getWidth()/2,inHandY-inHandBmp.getHeight()*2,null);  
        }else inHandBmp=null;
    } catch (Exception e) { Main.log(e); }}
    
	//############################################################################
	public void populateInventory() {if(inventory.size()==0)return; 
		int x = (int) ((menuX+invZone().x()) * zoom) ; int y = (int) ((menuY+invZone().y()) * zoom);
        for(InventoryObject obj : inventory) {
        	obj.setZone(new Zone(x,y,(int) (x+30 * zoom),(int) (y+30 * zoom),ZoneType.RECTANGLE ));
        	x+=30 * zoom;
        	if(x>(invZone().x()+invZone().w()-20) * zoom) {
        		x=(int) (invZone().x() * zoom);
        		y+=30 * zoom;
        	}
        }
	}
	//############################################################################methods
	//############################################################################
	public void addInvObject(InventoryObject obj) { Main.log("adding "+obj.title()+" in inventory bar");
		inventory.add(obj); 
		if(obj.selectedState==null)
			Main.instance.addAsh(new Ashe(obj.initialState().line.getId(),AshType.IS_IN_STATE,1));
	}
	//############################################################################
	public void removeItem(BasicObject basicObject) { Utils.log("removing  "+basicObject.title()+" from inventory bar");
		inventory.remove(basicObject); 
		if(inHand != null)
		Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
		inHand=null; inHandAnim=null; 
	}//############################################################################
	public void setZoom(float zoom) { try {
		 this.zoom=zoom; bg=null; 
	} catch (Exception e) { Main.log(e); }}
	
	//############################################################################
	//############################################################################events
	public boolean invObjectHasActed(Point clickPoint) { //sent by maingame
		if(inHand==null)
			return false;
		List<Action> actions = Action.getByDetector(inHand.selectedState );
		for(Action action:actions) {  
			if(Main.instance.actionMgr.canDoAction(action,Main.instance.player)) {  
				Main.instance.actionMgr.execute(action,Main.instance.player);
				Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
				inHand=null; inHandAnim=null; 
				return true;
			}
		
		}
		return false;
	}
	//############################################################################
	public boolean invObjectHasActedOnCharacter(Point p) { return false; } //useless for the moment
	
	//###################################################  
	private boolean handleClicInventoryBar(int x, int y) {  try {if(Main.instance.actionMgr.isfreezed())return false;
		for(InventoryObject obj : inventory) 
			if(obj.zone.isInZone(new Point((int) (x*Main.zoom),(int) (y*Main.zoom))))  { 
				if(inHand == null && inHandObj == null ) {  Utils.log("clicked on "+obj.title());
					Main.instance.addAsh(new Ashe(obj.line.getId(),AshType.HAS_IN_HAND,1));
					inHand=obj;
					inHandAnim=Animation.get(inHand.getSelectedState().animID());  
					return true;
				}
				if(inHand != null) {  Utils.log("clicked on "+obj.title()+" with "+inHand.title());
				
					if(inHand.line.getId() == obj.line.getId()) {
						Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
						inHand=null;inHandAnim = null;
						return false;
					}
					
					InventoryObject oldInHand = inHand;
					Main.instance.addAsh(new Ashe(obj.line.getId(),AshType.IS_CLICKED_ON,1));
					List<Action> actions = Action.getByDetector(obj.selectedState );
					for(Action action:actions)    
						if(Main.instance.actionMgr.canDoAction(action,Main.instance.player))   
							Main.instance.actionMgr.execute(action,Main.instance.player);
  
					Main.instance.removeAsh(new Ashe(obj.line.getId(),AshType.IS_CLICKED_ON,1));
					Main.instance.removeAsh(new Ashe(obj.line.getId(),AshType.HAS_IN_HAND,1));
					Main.instance.removeAsh(new Ashe(oldInHand.line.getId(),AshType.HAS_IN_HAND,1));
				 
				} 
			} 
		
		if(inHand != null) {  Utils.log("invzone clicked with "+inHand.title()); 
			Main.instance.removeAsh(new Ashe(inHand.line.getId(),AshType.HAS_IN_HAND,1));
			inHand=null;  
			inHandAnim=null;
			return false;
		}
	} catch (Exception e) { Main.log(e); }
	return false;  }
	  
	//###############################################################################################
	//############################################################################################### clic
	public boolean clic(int x, int y) {try {  
		 Point p = new Point( (int) (x-menuZone().x() ),(int) (y-menuZone().y() )) ; 
				if (!menuZone().isInZone(new Point(x, y))) { 
					Utils.log("outside menubar");
					return false;
				} 
				if (inHand != null && dragObjZone().isInZone(p) ) {
					invExplorer.setVisibility(View.VISIBLE); 
					freezed=true;   
				}
				else if (igMenuOpenerZone().isInZone( p)) {
					igMenu.goVisibility(true); 
					freezed=true;  	
				} else if (invZone().isInZone(p ))  
				 handleClicInventoryBar(x, y);
			  
				return true;
		} catch (Exception e) { Main.log(e); } return false;     
	}
	//###############################################################################################
	public boolean hasObject(int x, int y) { 
		 Point p = new Point( (int) (x-menuZone().x() ),(int) (y-menuZone().y() )) ;
		 if (invZone().isInZone(p ))  
				if(handleClicInventoryBar(x, y))return true;
		return false;
	}
	
	//###############################################################################################SQL values
	public String bgFolder(){return "games/"+Main .gameName+"/"+getGameInfo().getString("menubar", "bgname") ;}
	public Zone menuZone() { return Zone.get(getGameInfo().getInt("menubar","bgzone"));}
	public Zone invZone() { return Zone.get(getGameInfo().getInt("menubar","invzone"));}
	public Zone dragObjZone() { return Zone.get(getGameInfo().getInt("menubar","dragobjzone"));} 
	public Zone igMenuOpenerZone() { return Zone.get(getGameInfo().getInt("menubar","igmenuopenerzone"));} 
	/////////////////////////
	public static SQLLine getGameInfo() { return SQLTable.getTable("gameinfo").getLines().values().iterator().next(); }
}/////////////////////////////////////////////
