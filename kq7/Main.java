package com.example.kq7;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import agame.Ashe; 
import agame.Ashe.AshType;
import agame.GameCharacter;
import agame.GameCharacterStairManager;
import agame.MainGameActionManager;
import agame.MainGameObjectsManager;
import agame.MainGameTpManager;
import agame.NPGManager;
import agame.SoundPlayer;
import agame.Utils;
import agame.Utils.Pixel;
import agame.Utils.Point;
import agame.Utils.Position;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color; 
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup; 
import me.nina.gameobjects.Action;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.SpawnPoint;
import me.nina.gameobjects.Zone;

public class Main { 
	
	
	//##################################################################the thread:
	private boolean mContinuer;long durat ,outside , count =  0 ; 
	private long mLastExecutionTime = System.currentTimeMillis();
	private Handler runnable = new Handler();  
	private Runnable mUpdateRunnable = new Runnable() { @Override public void run() { try { 
		long currentTime = System.currentTimeMillis(); 
        long elapsedTime = currentTime - mLastExecutionTime; 
        outside+=elapsedTime;
        if(player!=null&&player.feetPosition!=null)centerScrollPaneOn(player.feetX(),player.feetY());
        else centerScrollPaneOn(actualChapter.positionStart().x,actualChapter.positionStart().y); 
        mContext.findViewById(R.id.videopanel).setBackgroundColor(Color.TRANSPARENT); 
        mContext.findViewById(R.id.menubar).setBackgroundColor(Color.TRANSPARENT); 
		actionMgr.run();
		npgMgr.run();
		if (player != null)  
			player.run(); 
		mContext.findViewById(R.id.videopanel) .invalidate();  
		mContext.findViewById(R.id.menubar).invalidate(); 
		long adjustedDelay = 100 - (System.currentTimeMillis()-currentTime);
		if (mContinuer) 
			runnable.postDelayed(this, Math.max(0, adjustedDelay));  
		mLastExecutionTime = System.currentTimeMillis();  
		//count ++;
		//durat+=System.currentTimeMillis()-currentTime;
		if(count>20) {
			 
			//Utils.log("outside of run = "+(outside/count));
			//Utils.log("run time = "+(durat/count)+" + delay="+adjustedDelay); 
			//durat=0;outside=0;count = 0 ;
		}
	} catch (Exception e) { log(e ); } } };

	//##################################################################the fields :
	 
	//managers
	public MainGameActionManager actionMgr; public MainGameObjectsManager objMgr;  
	public MainGameTpManager tpMgr; public NPGManager npgMgr;   
	
	//instances 
	public SoundPlayer soundPlayer; public static MenuBar menuBar; 
	public static MainActivity mContext; public static Main instance; 
	
	//values
	public static float zoom = 1f; public static String gameName ="game-name"; 
	public Scene actualSceneOnScreen; 
	
	//scene
	public List<Pixel> priorityArray; public Bitmap backgroundImage;  Ashe sceneExited; 
	  
	//game 
	public List<Ashe>currentAshes = new CopyOnWriteArrayList<Ashe>();  
	public GameCharacter player;
	
	//save
	Scene savedScenePosition; Point savedPlayerPosition;  CharacterState savedPlayerState; 
	List<Ashe>savedAshes = new CopyOnWriteArrayList<Ashe>();
	public Chapter actualChapter; 
	
	//############################################### construction :
	public Main(MainActivity mainActivity) {  mContext=mainActivity; instance=this;soundPlayer = new SoundPlayer();  }
 
	public void init() {log("init start"); try { 
			  Iterator<Chapter> iterator =  Chapter.getAll() .iterator();
			   
				 while(iterator.hasNext()) {
					 Chapter ch = iterator.next();
					 if(ch.number() == 0)
						 actualChapter=ch;
				 }
			  actionMgr = new MainGameActionManager(this);
			  objMgr= new MainGameObjectsManager(this);
			  tpMgr = new MainGameTpManager(this);
			  npgMgr=new NPGManager(this);
			  
	} catch (Exception e1) {log(e1 );  } log("init end"); }
	
	public void start() {log("starting");try { 
		  menuBar = (MenuBar) mContext.findViewById(R.id.menubar);
		  changeScene(actualChapter.startScene() );  
		  VideoPanel.calculateAndSetInitialZoom(); 
		  actionMgr.execute(actualChapter.firstAction(), null);if(player!=null&&player.feetPosition==null)player.setPos(new Point(100,100));
		  runLoop();log("started");
	  } catch (Exception e1) {log(e1 );  } }
	
	//############################################### methods :
	public void centerScrollPaneOn(int characterX, int characterY) {
	    int sceneWidth = backgroundImage.getWidth();
	    int sceneHeight = backgroundImage.getHeight();
	    int menuHeight = menuBar.menuZone().h(); // menu height TODO: handle different menu positions
	    View scrollv = mContext.findViewById(R.id.scrollv);
	    View scrollh = mContext.findViewById(R.id.scrollh);

	    int targetX = Math.max(0, (int)(characterX * zoom) - scrollh.getWidth() / 2);
	    int targetY = Math.max(0, (int)(characterY * zoom) - scrollv.getHeight() / 2 - menuHeight);

	    if ((int)(characterX * zoom) + scrollh.getWidth() / 2 > sceneWidth)
	        targetX = Math.max(0, sceneWidth - scrollh.getWidth()); // too much width?

	    if ((int)(characterY * zoom) + (scrollv.getHeight() / 2 - menuHeight) > sceneHeight)
	        targetY = Math.max(0, sceneHeight - scrollv.getHeight()); // too much height?

	    animateScroll(scrollh, targetX, 0);
	    animateScroll(scrollv, 0, targetY);
	}

	private void animateScroll(final View view, final int targetX, final int targetY) {
	    final int startX = view.getScrollX();
	    final int startY = view.getScrollY();
	    final long startTime = System.currentTimeMillis();
	    final long duration = 100; // Duration for the animation in ms

	    view.post(new Runnable() {
	        @Override
	        public void run() {
	            long elapsed = System.currentTimeMillis() - startTime;
	            float progress = Math.min(1, (float) elapsed / duration);

	            int currentX = (int) (startX + progress * (targetX - startX));
	            int currentY = (int) (startY + progress * (targetY - startY));

	            view.scrollTo(currentX, currentY);

	            if (progress < 1) {
	                view.post(this);
	            }
	        }
	    });
	}

	//###############################################
	public void changeScene(Scene destScene ) { Utils.log("change scene start");  
		 if(player!=null&&player.path!=null) {player.path.clear();player.path=null;} 

		if(sceneExited!=null) this.removeAsh(sceneExited);
		if(actualSceneOnScreen!=null) {
			sceneExited=new Ashe(actualSceneOnScreen.line.getId(),AshType.SCENE_WAS_EXITED,1);
			if(sceneExited!=null)this.addAsh(sceneExited);
		}

		changeScene(destScene,true );
ImgUtils.mappe.clear();
		if(player != null) {
			if(sceneExited!=null)//do exit actions if needed
				actionMgr.handleAction( Action.getByDetector(sceneExited.possessor() ),player); 

			//do enter actions if needed
			Ashe ashSceneEntered = new Ashe(actualSceneOnScreen.line.getId(),AshType.SCENE_HAS_BEEN_ENTERED,1);
			this.addAsh(ashSceneEntered);
			actionMgr.handleAction( Action.getByDetector(actualSceneOnScreen ),player);   
			this.removeAsh(ashSceneEntered);

			//record the visit
			Ashe ashSceneVisited = new Ashe(actualSceneOnScreen.line.getId(),AshType.SCENE_HAS_BEEN_VISITED,1);
			if(!this.haveAsh(ashSceneVisited))
				this.addAsh(ashSceneVisited);
			
			player.stairManager = new GameCharacterStairManager(this,player); 
			  
		}
		 Utils.log("change scene end");  
	}
	//###############################################
	public void changeScene(final Scene destScene, boolean dontDoAshJob) {  
		try {   
			actualSceneOnScreen = destScene; 
			if(player != null) player.actualScene = destScene;
 	            	 backgroundImage = ImgUtils.scal(actualSceneOnScreen.bgImage(),zoom); 
 			
			
			VideoPanel l = (VideoPanel) mContext.findViewById(R.id.videopanel); //resize
			ViewGroup.LayoutParams p = l.getLayoutParams(); 
			p.width = backgroundImage.getWidth();
			p.height = backgroundImage.getHeight(); 
			l.setLayoutParams(p);  
			 
			//new Thread(new Runnable() { @Override public void run() {//go faster in the scene 
			try {  
 	              priorityArray = Utils.getMatrix(actualSceneOnScreen, false);
			} catch (Exception e) { 
				priorityArray = null;
				log("no priority bg found for " + destScene.title());
			}
			//} }).start();
			 
			          
			  
		} catch (Exception e) { 
			priorityArray = null;
			log("no priority bg found for " + destScene.title());
		}
		
		objMgr.handleSceneChanged(); 
		

		new Thread(new Runnable() { @Override public void run() {
			soundPlayer.stopLoop();
			if(destScene.music().contains(".mid"))
            	 soundPlayer.playMidi(destScene.getFolder() + destScene.music(), actualSceneOnScreen.replaceSong(),actualSceneOnScreen.loopMusic()!=-998);
			else 
				if(  destScene.replaceSong()) {
					if(soundPlayer.isPlayingMidi())soundPlayer.stopMidi(); 
					soundPlayer.playAudio(destScene.getFolder()+destScene.music(), false,false,destScene.loopMusic());
				}
		
		} }).start();
		if(player!=null)player.stairManager = new GameCharacterStairManager(this,player); 
	}

	//##################################################################

	public void handleCharacterArrived(GameCharacter c) { 
		boolean wasFreeze = actionMgr.freezedWalk;
		if(c.equals(player))
		actionMgr.freezedWalk=false;
		tpMgr.handleCharacterArrived(c); 
		objMgr.handleCharacterArrived(c);
		npgMgr.handleCharacterArrived(c);
		if(!c.equals(player)||wasFreeze)for(Zone a:SpawnPoint.getByOwner(c.actualScene.line.getId()))
			if(Utils.getDistanceBetweenPoints(c.feetPosition, a.mid())<20) {
				addAsh(new Ashe(a.line.getId(),AshType.HAS_BEEN_COLLIDED,1));
				actionMgr.handleAction(Action.getByDetector(a),c);
				removeAsh(new Ashe(a.line.getId(),AshType.HAS_BEEN_COLLIDED,1));
			} 
	}
long durations = 0, counter =  0 ; 
	//##################################################################
	public void paint(Canvas g) {try {
		long start = System.currentTimeMillis();
			g.drawBitmap(backgroundImage,0,0,null);
			 
			if(!menuBar.freezed  ) { 
					objMgr.drawObjectsBackground(g);//if fullscreen obj show this+childs
				if(!objMgr.isFullScreen()){//or draw the obje we forced to zindex<50  
					npgMgr.paintImage(g);//then, paint player, characters, and object with own zindex
					objMgr.drawObjectsFront(g);//then draw the obje we forced to zindex>50
				}
				 
			}
			long end = System.currentTimeMillis();
			durations+=end-start;
			counter++;
			if(counter >=20) {
				log("total paint time="+(durations/counter) );
				 counter=0;durations=0; 
			}
	} catch (Exception e) { log(e ); } }
	//##################################################################
	public void clic(MotionEvent e) { try { 
		if (e.getAction() == MotionEvent.ACTION_UP) { 

			soundPlayer.busy=false;   
			if(menuBar.freezed || actionMgr.isfreezed()|| !objMgr.isFullScreen()&&actionMgr.freezedWalk || actionMgr.pendingAshesToSet.size()!=0) { 
				Utils.log("clic canceled by freezer"+actionMgr.freezedWalk); return ; }
			actionMgr.freezedWalk=false;//if fullscreen dont go to desired position
			if(!player.stairManager.isBusy())  {   
				player.zoneClicked = null;
				player.characterToRejoin=null;
				player.tpClicked=null;

				Point p = new Point((int) ((int) e.getX()/zoom),(int) ((int) e.getY()/zoom)); 
				if(!objMgr.isFullScreen())if(menuBar.invObjectHasActedOnCharacter(p)) return   ; 
				if(menuBar.invObjectHasActed(p)) return   ;  
				if(objMgr.objectHasBeenClicked(player,p))return  ;
				if(menuBar.inHand==null&&tpMgr.tpHasBeenClicked(p,player)) return  ;  
				if(!objMgr.isFullScreen())if(npgMgr.clickHasActedOnCharacter(p,player)) return   ; 
				 
				
				if(!Main.instance.objMgr.isFullScreen()) {
					 boolean press = true;
					 Main.instance.addAsh(new Ashe(Main.instance.actualSceneOnScreen.id,AshType.SCENE_HAS_BEEN_CLICKED,1));
					if(Main.instance.actionMgr.handleAction(Action.getByDetector(Main.instance.actualSceneOnScreen), Main.instance.player)) {
						Utils.log("clic cancel by scene press");
					 press=false;
					}
					Main.instance.removeAsh(new Ashe(Main.instance.actualSceneOnScreen.id,AshType.SCENE_HAS_BEEN_CLICKED,1)); 
					 
					for(Zone sp:SpawnPoint.getByOwner(Main.instance.actualSceneOnScreen.id))
						if(sp.isInZone(p)) {
							Main.instance.addAsh(new Ashe(sp.id,AshType.ZONE_HAS_BEEN_CLICKED,1));
							if(Main.instance.actionMgr.handleAction(Action.getByDetector(sp), Main.instance.player)) {
								Utils.log("clic cancel by spawn press : "+sp.title());
	 							 press=false;
							}
							Main.instance.removeAsh(new Ashe(sp.id,AshType.ZONE_HAS_BEEN_CLICKED,1));
						}
				if(press)Main.instance.player.setClicPose(p); 
				}
				
				
				
			} 

		} 
	} catch (Exception e1) { log(e1 ); }  }

	//##################################################################
	private void runLoop() {mContinuer=true; runnable.post(mUpdateRunnable); } 
	public void stopLoop() { mContinuer = false; runnable.removeCallbacks(mUpdateRunnable); }
	public static void log(Exception e) { log(e.getMessage()+" "); log(e.getStackTrace()); }
	public static void log(StackTraceElement[] stackTrace) { for(StackTraceElement a : stackTrace ) log( a.toString());  }
	public static void log(String s) { Log.d("kq7",s);   }
	public boolean haveAsh(Ashe ash) {  return currentAshes.contains(ash); }
	public void addAsh(Ashe ash) {   currentAshes.add(ash);  }

	//##################################################################
	public void removeAsh(Ashe ash) { 
		int contain = 0; int index=-1; int i = 0;
		for(Ashe a : currentAshes){ 
			if(a!=null&&a.equals(ash)){
				contain++;
				index = i; 
			}
			i++;
		}
		if(index!=-1 && contain !=1) 
			currentAshes.remove(index);   
		else 
			currentAshes.remove(ash);  
	} 
	//##################################################################

	public int howManyAsh(Ashe ash){
		int i = 0;
		for(Ashe a : currentAshes) 
			if(a!=null&&a.equals(ash)) 
				i++; 
		return i;
	} 
 
	//############################################### 
	//need to be rewritten if future 
	//TODO :
	public void saveBeforeDead(){
		savedAshes = new CopyOnWriteArrayList<>( currentAshes);
		savedAshes.add(new Ashe(player.id,AshType.WILL_BE_SPAWNED,1));
		savedScenePosition = this.actualSceneOnScreen;
		savedPlayerPosition = player.feetPosition; 
		savedPlayerState=player.getSelectedStat(); 
		saveToFile();
	}
	public void restoreBeforeDead(){actionMgr.freezedWalk=false;
		menuBar.inHand=null;this.npgMgr.allThePnj.clear();player=null;
		menuBar.inventory.clear();GameObject.reset();
		currentAshes = new CopyOnWriteArrayList<>(savedAshes); 
		for(Ashe a : currentAshes){
			if(a.type() == AshType.HAS_IN_HAND||a.type() == AshType.SCENE_HAS_BEEN_ENTERED||a.type() == AshType.IS_CLICKED_ON) {
				currentAshes.remove(a);continue;
			}
			if(a.type() == AshType.HAS_IN_INVENTORY)
				menuBar.inventory.add((InventoryObject) a.possessor());
			if(a.type()==AshType.IS_IN_STATE ) { 
				if(a.possessor().possessorObj() instanceof BasicObject)
				((BasicObject)a.possessor().possessorObj()).setSelectedState((ObjectState) a.possessor());
				else currentAshes.remove(a);
			}
			if(a.type() == AshType.WILL_BE_SPAWNED) {
				npgMgr.spawnPnj(a);
				currentAshes.remove(a);
	}}
		this.changeScene(savedScenePosition,true); 
		player.setPos(savedPlayerPosition); 
		player.setSelectedStat(savedPlayerState);
		centerScrollPaneOn(player.feetX(), player.feetY());
	} 
	public void saveToFile(  ) {
		SharedPreferences sharedPreferences = mContext.getSharedPreferences("game_prefs", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		clearSavedData();
		int i = 0 ;
		editor.putString(new Ashe(player.id,AshType.WILL_BE_SPAWNED,1).toSqlValue()+":"+i, new Ashe(player.id,AshType.WILL_BE_SPAWNED,1).toSqlValue());
		i++;
		for (Ashe ash : currentAshes ) {
			editor.putString(ash.toSqlValue()+":"+i, ash.toSqlValue());
			i++;
		}

		editor.putInt("scene", savedScenePosition.line.getId());
		editor.putString("pos", savedPlayerPosition.x +","+savedPlayerPosition.y);
		editor.putInt("state", savedPlayerState.line.getId());

		for (Entry<Action, Long> a : actionMgr.currentTimers.entrySet()) 
			editor.putLong(new Ashe(a.getKey().line.getId(),AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK,0).toSqlValue(), a.getValue());

		editor.apply();
	}
 
	public void clearSavedData() {
	    SharedPreferences sharedPreferences = mContext.getSharedPreferences("game_prefs", Context.MODE_PRIVATE);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
	    editor.clear(); editor.commit();  
	}
	
	public void restoreGame( ) { try {
		SharedPreferences sharedPreferences = mContext.getSharedPreferences("game_prefs", Context.MODE_PRIVATE);
		if( sharedPreferences.getAll().size() == 0)return;

		actionMgr.pendingAshesToSet.clear();currentAshes.clear();   
		menuBar.inHand=null;
		menuBar.inventory.clear();
		savedAshes = new CopyOnWriteArrayList<>();

		for (Map.Entry<String, ?> e : sharedPreferences.getAll().entrySet()) {
			String key = e.getKey();
			if (!key.equals("scene") && !key.equals("pos") && !key.equals("state")) {
				Ashe ash = Ashe.fromSqlValue(key);
				if(ash.type()==AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK)
					actionMgr.currentTimers.put( (Action) ash.possessor() ,  (Long) e.getValue() );
				else
					savedAshes.add(ash );
			}
		} 
 
		savedScenePosition = Scene.get(sharedPreferences.getInt("scene", 0));
		String pos = sharedPreferences.getString("pos", "");
		savedPlayerPosition = new Position(Integer.parseInt(pos.split(",")[0]),Integer.parseInt(pos.split(",")[1]),PosType.BOTTOM.getValeur());
		savedPlayerState = CharacterState.get(sharedPreferences.getInt("state", 0)); 

		restoreBeforeDead() ;
	} catch (Exception e1) { log(e1 ); }}  
}