package agame;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set; 
 
import com.example.kq7.Main;

import agame.Utils.Point;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.SparseIntArray;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType; 

public class Utils { 
 
	//##################################################priorities
	static Map<Integer,List<Pixel>> mappe = new HashMap<>();
 
	public static List<Pixel> getMatrix(Scene scene, boolean reset) {
	    Bitmap img = scene.priorityBgImage();
	    if (img == null) return null;
	    if (reset) mappe.remove(scene.line.getId());

	    if (!mappe.containsKey(scene.line.getId())) {
	        int width = img.getWidth();
	        int height = img.getHeight();
	        int[] pixels = new int[width * height];
	        img.getPixels(pixels, 0, width, 0, 0, width, height);

	        SparseIntArray colorCountMap = new SparseIntArray();
	        Map<Integer, Pixel> uniquePixelMap = new HashMap<>();
	        Map<Integer, Set<Point>> pointsMap = new HashMap<>();
	        Map<Integer, Integer> records = new HashMap<>();

	        // Count the colors
	        for (int pixel : pixels) {
	            int count = colorCountMap.get(pixel, 0);
	            colorCountMap.put(pixel, count + 1);
	        }

	        // Find the most frequent color
	        int mostFrequentColor = 0;
	        int maxCount = 0;
	        for (int i = 0; i < colorCountMap.size(); i++) {
	            int color = colorCountMap.keyAt(i);
	            int count = colorCountMap.valueAt(i);
	            if (count > maxCount) {
	                maxCount = count;
	                mostFrequentColor = color;
	            }
	        }

	        // Clear the color count map
	        colorCountMap.clear();

	        // Process pixels again
	        for (int i = 0; i < width; i++) {
	            for (int j = 0; j < height; j++) {
	                int index = i + j * width;
	                int rgb = pixels[index];

	                if (!records.containsKey(rgb)) {
	                    String zindexString = scene.zindexes().get(rgb + "");
	                    if (zindexString != null) {
	                        int zindex = Integer.parseInt(zindexString);
	                        records.put(rgb, zindex);
	                    } else {
	                        records.put(rgb, -998);
	                        continue;
	                    }
	                }

	                int stored = records.get(rgb);

	                if (rgb != mostFrequentColor && !uniquePixelMap.containsKey(rgb)) {
	                    Pixel pixel = new Pixel(stored, rgb);
	                    uniquePixelMap.put(rgb, pixel);
	                }

	                Set<Point> points = pointsMap.get(rgb);
	                if (points == null) {
	                    points = new HashSet<>();
	                    pointsMap.put(rgb, points);
	                }
	                points.add(new Point(i, j));
	            }
	        }

	        for (Map.Entry<Integer, Pixel> entry : uniquePixelMap.entrySet()) {
	            int pixel = entry.getKey();
	            Set<Point> points = pointsMap.get(pixel);
	            entry.getValue().points = points;
	        }

	        mappe.put(scene.line.getId(), new ArrayList<>(uniquePixelMap.values()));
	    }
	    return mappe.get(scene.line.getId());
	}
 
 
 
	//##################################################
	public static void log(String s) {   
		Main.log(s);
		 
	} 
	public static List<Point> getRectanglePoints(Point a, Point b) {
        List<Point> points = new ArrayList<>();

        int minX = Math.min(a.x, b.x);
        int maxX = Math.max(a.x, b.x);
        int minY = Math.min(a.y, b.y);
        int maxY = Math.max(a.y, b.y);

        for (int i = minX; i <= maxX; i++) {
            for (int j = minY; j <= maxY; j++) {
                points.add(new Point(i, j));
            }
        }

        return points;
    }//##################################################
	public static boolean isCollided(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
	    int o1 = orientation(x1, y1, x2, y2, x3, y3);
	    int o2 = orientation(x1, y1, x2, y2, x4, y4);
	    int o3 = orientation(x3, y3, x4, y4, x1, y1);
	    int o4 = orientation(x3, y3, x4, y4, x2, y2);//dont work

	    if (o1 != o2 && o3 != o4) {
	        return true;  
	    }

	    return false;  
	}
	//##################################################
	private static int orientation(int x1, int y1, int x2, int y2, int x3, int y3) {
	    int val = (y2 - y1) * (x3 - x2) - (x2 - x1) * (y3 - y2);
	    if (val == 0) return 0;  
	    return (val > 0) ? 1 : 2;  
	}//##################################################
	public static List<Point> getArcPoints(int x1, int y1, int x3, int y3, int x2, int y2) {
        List<Point> points = new ArrayList<>();
 
        for (float t = 0; t <= 1; t += 0.005) {
            float x = (1 - t) * (1 - t) * x1 + 2 * (1 - t) * t * x2 + t * t * x3;
            float y = (1 - t) * (1 - t) * y1 + 2 * (1 - t) * t * y2 + t * t * y3;
            points.add(new Point((int) x, (int) y));
             
        }

        return points;
    }

	//##################################################
	public static List<Point> getOvalePoints(int x1, int y1, int x2, int y2) {
        List<Point> points = new ArrayList<>();
 
        int centerX = (x1 + x2) / 2;
        int centerY = (y1 + y2) / 2;
        int a = Math.abs(x2 - x1) / 2;
        int b = Math.abs(y2 - y1) / 2;
 
        int minX = centerX - a;
        int minY = centerY - b;
        int maxX = centerX + a;
        int maxY = centerY + b;

        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                if (isPointInsideOval(x, y, centerX, centerY, a, b)) {
                    points.add(new Point(x, y));
                }
            }
        }

        return points;
    }
	//##################################################
	private static boolean isPointInsideOval(int x, int y, int centerX, int centerY, int a, int b) { 
        double value = Math.pow((x - centerX) / (double) a, 2) + Math.pow((y - centerY) / (double) b, 2);
        return value <= 1;
    }
	//##################################################
	public static List<Point> getUnfilledOvale(int x1, int y1, int x2, int y2) {
	    List<Point> points = new ArrayList<>();
 
	    int centerX = (x1 + x2) / 2;
	    int centerY = (y1 + y2) / 2;
	    int a = Math.abs(x2 - x1) / 2;
	    int b = Math.abs(y2 - y1) / 2;

	    for (double angle = 0; angle <= 2 * Math.PI; angle += 0.01) {
	        int pointX = (int) (centerX + a * Math.cos(angle));
	        int pointY = (int) (centerY + b * Math.sin(angle));
	        points.add(new Point(pointX, pointY));
	    }

	    return points;
	}
	//##################################################
    public static List<Point> getUnfilledRectangle(Point a, Point b) {
        List<Point> points = new ArrayList<>();

        int minX = Math.min(a.x, b.x);
        int maxX = Math.max(a.x, b.x);
        int minY = Math.min(a.y, b.y);
        int maxY = Math.max(a.y, b.y);
 
        for (int i = minX; i <= maxX; i++) {
            points.add(new Point(i, minY));  
            points.add(new Point(i, maxY));  
        }

        for (int j = minY + 1; j < maxY; j++) {
            points.add(new Point(minX, j));  
            points.add(new Point(maxX, j));  
        }

        return points;
    }
  //##################################################
    public static List<Point> getLinePoints(int x1, int y1, int x2, int y2) {
        List<Point> points = new ArrayList<>();

        int deltaX = Math.abs(x2 - x1);
        int deltaY = Math.abs(y2 - y1);
        int signX = x1 < x2 ? 1 : -1;
        int signY = y1 < y2 ? 1 : -1;

        int erreur = deltaX - deltaY;

        int x = x1;
        int y = y1;

        while (true) {
            points.add(new Point(x, y));

            if (x == x2 && y == y2) {
                break;
            }

            int erreurDouble = 2 * erreur;

            if (erreurDouble > -deltaY) {
                erreur -= deltaY;
                x += signX;
            }

            if (erreurDouble < deltaX) {
                erreur += deltaX;
                y += signY;
            }
        }

        return points;
    }

  //##################################################
    public static int getDistanceBetweenPoints(Point p1, Point p2) {
        int deltaX = p2.x - p1.x;
        int deltaY = p2.y - p1.y; 
        double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY); 
        return (int) Math.round(distance);
    }
    
  //##################################################
    public static Zone getZoneFromPoint(Point p, List<Zone> l, int tolerance) {
    	for(Zone line : l) {  
            if( line.isNear(p, tolerance))return line ;
        }
        return null;
    }

	//##################################################
	public static Zone getProlongedLine(int clicX, int clicY, int posX, int posY) {
		int endX = clicX;
		int endY = clicY; 
		if (posY > endY) 
			endY = posY - (int) (Math.abs(endY - posY) * 1000 / Math.sqrt((endX - posX)*(endX - posX) + (endY - posY)*(endY - posY)));
		else if (posY < endY) 
			endY = posY + (int) (Math.abs(endY - posY) * 1000 / Math.sqrt((endX - posX)*(endX - posX) + (endY - posY)*(endY - posY))); 
		return new Zone(posX, posY, endX, endY, ZoneType.LINE );//prolonged line
	}
	//##################################################
	public static class Pixel {
		    public final int stored;
		    public final int rgb;
			public Set<Point> points;//TODO: need to be final

		    public Pixel(int stored, int rgb) {
		        this.stored = stored;
		        this.rgb = rgb;
		    } 
		    @Override public boolean equals(Object obj) {
		        if (this == obj) return true;
		        if (!(obj instanceof Pixel)) return false;
		        Pixel other = (Pixel) obj;
		        return stored == other.stored && rgb == other.rgb;
		    }

		    @Override public int hashCode() {
		        return Objects.hash(stored, rgb);
		    }

			
		}
	public static boolean isInRectangle(Point p, int x1, int y1, int x2, int y2) {
		return isInRectangle(p,x1,y1,x2,y2,0);
	}
	public static boolean isInRectangle(Point p, int x1, int y1, int x2, int y2, int tolerance) {
	    int rectX = Math.min(x1, x2) - tolerance;
	    int rectY = Math.min(y1, y2) - tolerance;
	    int rectWidth = Math.abs(x2 - x1) + 2 * tolerance;
	    int rectHeight = Math.abs(y2 - y1) + 2 * tolerance;
	    
	    return p.x >= rectX && p.x <= rectX + rectWidth && p.y >= rectY && p.y <= rectY + rectHeight;
	}

	public static boolean isInOval(Point p, int x1, int y1, int x2, int y2) {
		return isInOval(p,x1,y1,x2,y2,0);
	}
	public static boolean isInOval(Point p, int x1, int y1, int x2, int y2, int tolerance) {
	    int centerX = (x1 + x2) / 2;
	    int centerY = (y1 + y2) / 2;
	    int a = (Math.abs(x2 - x1) + 2 * tolerance) / 2;
	    int b = (Math.abs(y2 - y1) + 2 * tolerance) / 2;

	    double dx = (p.x - centerX) * 1.0 / a;
	    double dy = (p.y - centerY) * 1.0 / b;

	    return dx * dx + dy * dy <= 1;
	}
	//##################################################
	public static class Position extends Point {
		public final PosType pos;
		public Position(int x, int y,int z) {super(x, y);this.pos=PosType.from(z);}
		public Position(String s) {super(Integer.parseInt(s.split(",")[0]),Integer.parseInt(s.split(",")[1]));
			this.pos=PosType.from(s.split(",")[2]);} 
	}//##################################################
	public static class Point{public final int x,y;
	public Point(int x,int y){this.x=x;this.y=y;}@Override
	public int hashCode() { final int prime = 31;
	int result = 1; result = prime * result + x; result = prime * result + y; return result; }
	@Override public boolean equals(Object o) { Point ot = (Point) o; return x==ot.x&&y==ot.y; }}
	
	//##################################################
	public static Bitmap unprioritize(Bitmap movingImage, int addx, int addy, int posY, boolean hasStair, GameCharacter charact) {
	    List<Integer> stList = null;
	    if (hasStair) stList = charact.stairManager.getHiddenRgb();
	    
	    int width = movingImage.getWidth();
	    int height = movingImage.getHeight();
	    int[] pixels = new int[width * height];
	    movingImage.getPixels(pixels, 0, width, 0, 0, width, height);

	    for (int y = 0; y < height; y++) {
	        for (int x = 0; x < width; x++) {
	            int index = y * width + x;
	            if (pixels[index] != Color.TRANSPARENT) {
	                for (Pixel pixel : Main.instance.priorityArray) {
	                    final int stored = pixel.stored;
	                    final int rgb = pixel.rgb;
	                    if ((hasStair && stList.contains(rgb)) || (!hasStair && stored > posY)) {
	                        if (pixel.points.contains(new Point(x + addx, y + addy))) {
	                            pixels[index] = Color.TRANSPARENT;
	                            break;
	                        }
	                    }
	                }
	            }
	        }
	    }

	    movingImage.setPixels(pixels, 0, width, 0, 0, width, height);
	    return movingImage;
	}
	/*	public static Bitmap unprioritize(Bitmap movingImage, int addx, int addy, int posY, boolean hasStair, GameCharacter charact) {
		List<Integer> stList = null;
		if(hasStair)stList = charact.stairManager.getHiddenRgb();
		
		int[] pixels = new int[movingImage.getWidth() * movingImage.getHeight()];
		movingImage.getPixels(pixels, 0, movingImage.getWidth(), 0, 0,
				movingImage.getWidth(), movingImage.getHeight());

		HashSet<Point> pix = new HashSet<>();
		for (int y = 0; y < movingImage.getHeight(); y++) {
		    for (int x = 0; x < movingImage.getWidth(); x++) {
		        if (pixels[y * movingImage.getWidth() + x] != Color.TRANSPARENT) {
		            pix.add(new Point(x + addx, y + addy));
		        }
		    }
		}
		
			if(Main .instance.priorityArray!=null )
				for (Pixel pixel : Main.instance.priorityArray) {
					final int stored = pixel.stored;    final int rgb = pixel.rgb;
					if ( (hasStair && stList.contains(rgb))|| (!hasStair && stored > posY ))  { 
				        for (Point p : pix)  
				            if (pixel.points.contains(p))  
				            	 movingImage.setPixel(p.x - addx, p.y - addy, Color.TRANSPARENT);
				}}
			return movingImage;
	}
	 
	*/
}