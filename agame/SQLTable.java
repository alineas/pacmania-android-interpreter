package agame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.example.kq7.Main;
import com.example.kq7.MainActivity;

import android.database.Cursor;
 

public class SQLTable {
	private static Map <String,SQLTable> tables = new HashMap<String, SQLTable>(); 
	final Map<Integer,SQLLine> lines = new HashMap< >(); 
	 long total=0;
	//##################################################
	 public SQLTable(String name) {
		    Cursor cursor = null;
		    final Set<String> columns = new HashSet<>();
		    try { 
		        cursor = MainActivity.sql.myDataBase.query(name, null, null, null, null, null, null);

		        if (cursor != null && cursor.moveToFirst()) { 
		            String[] columnNames = cursor.getColumnNames();
		            for (String columnName : columnNames) {
		                columns.add(columnName);
		            }
 
		            do {
		            	 int id = cursor.getInt(cursor.getColumnIndex("id"));
		                 Map<String, String> rowData = new HashMap<>();
		                 for (String col : columns) {
		                     rowData.put(col, cursor.getString(cursor.getColumnIndex(col)));
		                 }
		                 lines.put(id, new SQLLine(name, columns, id, rowData));
		            } while (cursor.moveToNext());
		        }
		    } catch (Exception e) {
		        Main.log(e);
		    } finally {
		        if (cursor != null) {
		            cursor.close();
		        }
		    }
		}
	//##################################################
	public SQLLine getById(int id) { 
		return lines.get(id);
	}
	//##################################################
	public static SQLTable getTable(String tableName) { tableName=tableName.replaceAll("'", "`");
		if(!tables.containsKey(tableName)) {
			long l = System.currentTimeMillis();
			SQLTable tb = new SQLTable(tableName);
			Utils.log(""+(System.currentTimeMillis()-l));
			tables.put(tableName, tb);
		}
		return tables.get(tableName);
	} 
	//##################################################
	public Map<Integer, SQLLine> getLines() { 
		return lines ; }  

 
}