package agame;

import java.util.LinkedList;
import java.util.Objects;

import me.nina.gameobjects.GameObject; 

public class Ashe  {
	 
	private String title;
	final int possessor ;
	final AshType type;
	final int nbr;
	
	public static  enum AshType {
		HAS_BEEN_COLLIDED(0), HAS_BEEN_DISABLED(1),WILL_BE_DISABLED(2), IS_IN_STATE(3),  WILL_BE_IN_STATE(4), 
	     HAS_IN_HAND(5), SCENE_HAS_BEEN_VISITED(6), HAS_IN_INVENTORY(7),WILL_HAVE_IN_INVENTORY(8), 
	     IS_CLICKED_ON(9), ACTUAL_POSITION(10),WILL_BE_IN_POSITION(11), 
	     WILL_DO_AFTER_TIMER(12), WILL_DO_WITH_RANDOM(13), 
	     RECORD_ACTUAL_POSITION(14), WILL_BE_IN_POSITION_RECORDED(15), 
	     OR(16), HAS_BEEN_SAID(20),WILL_SAY(21), WILL_HAVE_IN_HAND(22),
	     SCENE_WAS_EXITED(24), WILL_BE_SPAWNED(25), WILL_BE_CLICKED(26),
	     SCENE_HAS_BEEN_ENTERED(27), WILL_BE_DESPAWN(28), HAS_BEEN_CLICKED(29),
	     IS_THE_ACTOR(30), ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK(31),
	     IS_ARRIVED(32), HAS_BEEN_SPAWNED(33),
	     WILL_LOOK_RIGHT(34), WILL_LOOK_LEFT(35), WILL_LOOK_TOP(36), WILL_LOOK_BOTTOM(37),
	     WILL_BE_LOOKED(38), WILL_INSIDE_OUTSIDE_THIS_STATE(39), WILL_INSIDE_THIS_STATE(40),
	     WILL_OUTSIDE_THIS_STATE(41) , WILL_INSIDE_WAIT_AND_OUTSIDE_STATE(17), 
	     WILL_LOOK_BOT_RIGHT(42), WILL_LOOK_BOT_LEFT(43), WILL_LOOK_TOP_RIGHT(44), WILL_LOOK_TOP_LEFT(45),
	     IS_ACTUAL_SCENE(46), HAS_NOTHING_IN_HAND(47), SAVE_BEFORE_DEAD(48),
	     RESTORE_BEFORE_DEAD(49), WILL_SAY_UNFREEZE(50),
	     ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK(51) ,ACTION_CAN_BE_EXECUTED(52), ENTER_CHAPTER(53),
	     SCENE_HAS_BEEN_CLICKED(54),IS_IN_ZONE(55), ZONE_HAS_BEEN_CLICKED(56); 
	    private final int valeur; 
	    AshType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; }  
	    public static AshType from(String s) {  return from(Integer.parseInt(s));  }
	    public static AshType from(int valeur) {
	        for (AshType type : values())  
	            if (type.getValeur() == valeur) return type; 
	        throw new IllegalArgumentException("no ashe with value : " + valeur);
	    } 
	}
 
	 public Ashe (int g, int v, int n ){ possessor = g; type = AshType.from(v); nbr = n; } 
	public Ashe(int g, AshType t, int n) { possessor = g; type = t; nbr = n; }



	public AshType type() {  return type; }
	public GameObject possessor() {  return GameObject.getById(possessor); } 
	public int getNbr() {  return nbr; }
	
	public static LinkedList <Ashe> getlist(String string) {
		LinkedList <Ashe> temp = new LinkedList<Ashe>(); 
		for(String a : string.split(";"))if(!a.equals(""))
			temp.add(fromSqlValue(a));
		return temp;
	}
	
	@Override public boolean equals(Object obj) {
	    if (!(obj instanceof Ashe)) return false;
	    Ashe other = (Ashe) obj;
	    return Objects.equals(possessor, other.possessor) &&
	           Objects.equals(title, other.title) &&
	           type == other.type;
	}
  
	public static Ashe fromSqlValue(String s){
		String[] split = s.split(":");  
		return new Ashe( Integer.parseInt(split[0]) ,
				Integer.parseInt(split[1]),
				Integer.parseInt(split[2]));
	}
	
	
	public String toSqlValue(){ 
		return possessor +":"+type.getValeur()+":"+nbr;
	}
	
	
	public String title(){
		if(possessor!=-1)
		 return possessor().title()+" "+type;
		 return type+"";
		 
	}
	
	
	
	
}
