package agame;

 
import java.io.IOException;
import java.io.InputStream;

import com.example.kq7.Main;
import com.leff.midi.MidiFile;
import com.leff.midi.event.ChannelAftertouch;
import com.leff.midi.event.ChannelEvent;
import com.leff.midi.event.Controller;
import com.leff.midi.event.MidiEvent;
import com.leff.midi.event.NoteAftertouch;
import com.leff.midi.event.NoteOff;
import com.leff.midi.event.NoteOn;
import com.leff.midi.event.PitchBend;
import com.leff.midi.event.ProgramChange;
import com.leff.midi.event.meta.Tempo;
import com.leff.midi.util.MidiEventListener;
import com.leff.midi.util.MidiProcessor;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer; 
import cn.sherlock.com.sun.media.sound.SF2Soundbank;
import cn.sherlock.com.sun.media.sound.SoftSynthesizer;
import jp.kshoji.javax.sound.midi.MetaMessage;
import jp.kshoji.javax.sound.midi.Receiver;
import jp.kshoji.javax.sound.midi.ShortMessage;

 
public class SoundPlayer { 
	MediaPlayer midiPlayer = new MediaPlayer(); 
	public static SoftSynthesizer synth;
	public static Receiver recv; 
	private MidiProcessor midiCustom; 
	private Looper actualLoop;
	
	static AssetManager assets;
	
	public boolean busy,loopMidi=false;
	public String actualMidiPath="";
	
	private String oldSequence,actualSeq; 
	public MediaPlayer actualPlayer;
	  
	public static float fxVolume=0.6f;
	public static float midiVolume=60;
	
	//####################################################################
	public SoundPlayer() {
		assets = Main.mContext.getAssets();
		
		midiPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override public void onCompletion(MediaPlayer mp) {
					if (oldSequence != null)  playOldSequence();  } });
		
		new Thread(new Runnable() {  @Override public void run() { try {
			SF2Soundbank sf = new SF2Soundbank(assets.open("soundfont.sf2"));
			synth = new SoftSynthesizer(); 
			synth.open(); 
			synth.loadAllInstruments(sf);
			synth.getChannels()[0].programChange(0);
			synth.getChannels()[1].programChange(1);
			recv = synth.getReceiver(); Main.log("sound loaded");
			midiPlayer.setOnCompletionListener(null);midiPlayer=null;
		} catch ( Exception e) { e.printStackTrace();Main.log(e);}} }).start(); }
	
	//####################################################################
	public void playMidi(String path, boolean replaceCurrent, boolean loop) {try{
		  loopMidi=loop;
		  if(!replaceCurrent)oldSequence = path;
		  else actualSeq=path;
		  if(recv!=null&&(midiCustom!=null&&!midiCustom.isRunning()|| midiCustom==null || replaceCurrent ) ) {
			  openCustom(path); midiCustom.start(); 
		  } else if (midiPlayer!=null&&!midiPlayer.isPlaying() || replaceCurrent) {
			  openSound(midiPlayer,path);  midiPlayer.start(); }
	} catch ( Exception e) { Main.log( e);   }  }
	
	//#################################################################### 
	public void playOldSequence() {try {   
		if(recv!=null) { 
			if(loopMidi) { openCustom(actualSeq); midiCustom.start();  
			}else { openCustom(oldSequence); midiCustom.start(); }    
	    	 
		}else { 
			if(loopMidi) { midiPlayer.stop(); midiPlayer.seekTo(0); midiPlayer.start(); 
    		} else { openSound(midiPlayer,oldSequence);  midiPlayer.start(); }
		 }
	} catch ( Exception e) { Main.log(e); }}
	
	//####################################################################
	static void openSound(MediaPlayer p, String s) throws IOException {
		p.reset();
		AssetFileDescriptor assetFileDescriptor = assets.openFd(s);
		p.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
		assetFileDescriptor.close(); 
		p.prepare();
	}
	private void openCustom(String path) throws Exception { 
		if(midiCustom!=null)  this.stopMidi();
		InputStream file = assets.open(path);
		midiCustom = new MidiProcessor(new MidiFile(file)); 
	    midiCustom.registerEventListener(new EventPrinter( recv,synth), MidiEvent.class); 
	      Main.log("old seq"+oldSequence+midiCustom);
	      file.close();
	}
	//####################################################################
	 public void playAudio(final String path, final boolean fx, final boolean freeze ) { playAudio(path, fx, freeze,-998 );  }
	 public void playAudio(final String path, final boolean fx, final boolean freeze,int loopTime ) {
	    	if(loopTime != -998) { actualLoop= new Looper(path,loopTime);return;}
	    	final MediaPlayer player = new MediaPlayer(); 
	    	new Thread(new Runnable() { @Override public void run() { try {
	                    openSound(player,path);
	                    if (fx) player.setVolume(fxVolume, fxVolume);
	                    else if(freeze) busy = true; //its a message sor mark it for wait
	                    player.start();
 
	                    while (player.isPlaying()) { 
	                    	if(!busy&&!fx&&freeze)
	                    		player.stop();
	                        try { Thread.sleep(100);   } catch ( Exception e) { Main.log(e); }
	                    }
	                    if( !fx && freeze)busy = false;//only freezers can defreeze
	                    player.release();
	                     
	                } catch ( Exception e) { Main.log("trying to load sound "+path);Main.log(e); }  } }).start();
	    	 actualPlayer = player;
	    } 
	//####################################################################
	  void sendMidiMsg(int command, int channel, int data1, int data2, long timeStamp) { try {
	            ShortMessage message = new ShortMessage();
	            message.setMessage(command, channel, data1, data2);
	            recv.send(message, timeStamp);
	  } catch (Exception e) {  Main.log(e);  }  }
	//####################################################################
	    public boolean isPlayingMidi() {  
	    	return midiPlayer!=null&&midiPlayer.isPlaying()||midiCustom!=null&&midiCustom.isRunning(); }
	  //####################################################################
		public void stopMidi() { 
			if(recv != null) { Main.log("stop midi");
				for (int channel = 0; channel < 16; channel++) {
				    for (int note = 0; note < 128; note++) {
				    	sendMidiMsg(ShortMessage.NOTE_OFF, channel, note, 0, -1);
				    	sendMidiMsg(ShortMessage.CONTROL_CHANGE, channel, 64, 0, -1);
				    }
				}  
					midiCustom.stop();
					midiCustom.unregisterAllEventListeners();
					midiCustom.reset();
				 
			
			}else if(midiPlayer!=null) {
				midiPlayer.stop(); midiPlayer.release();
			}
		}
		//####################################################################
		public void stopLoop() { 
			if(actualLoop!=null)actualLoop.stop();actualLoop=null; }
		//####################################################################
		private void adjustMidiVolume(int channel, int volume) { try { 
			sendMidiMsg(ShortMessage.CONTROL_CHANGE, channel, 7, (int) (volume * 1.20), -1);
	        sendMidiMsg(ShortMessage.CONTROL_CHANGE, channel, 64, 0, -1);
		    } catch ( Exception e) { Main.log(e); }
		}
		//####################################################################
		public void adjustMidiVolume(int volume) { 
			if(midiPlayer!=null)midiPlayer.setVolume(volume, volume);  
        	midiVolume=volume;
			if(recv==null)return;
		    for (int channel = 0; channel < 16; channel++)  
		    	adjustMidiVolume(channel, volume); 
		}
}
//####################################################################
//####################################################################
class Looper { 
    boolean loop=true;   
    public Looper(String path, final int loopTime) { try {
        final MediaPlayer mediaPlayer1 = new MediaPlayer(),mediaPlayer2 = new MediaPlayer(); 
        SoundPlayer.openSound(mediaPlayer1,path); SoundPlayer.openSound(mediaPlayer2,path);
        mediaPlayer1.start(); 
    	new Thread(new Runnable() { @Override public void run() {
            	long duration1 = mediaPlayer1.getDuration(), duration2 = mediaPlayer2.getDuration(); 
                while (loop) { 
                    long remainingTime1 = duration1 - mediaPlayer1.getCurrentPosition();
                    long remainingTime2 = duration2 - mediaPlayer2.getCurrentPosition(); 
                    if (remainingTime2 <= loopTime && !mediaPlayer1.isPlaying()) {
                        mediaPlayer1.seekTo(0); mediaPlayer1.start(); } 
                    if (remainingTime1 <= loopTime && !mediaPlayer2.isPlaying()) {
                        mediaPlayer2.seekTo(0); mediaPlayer2.start(); } 
                    try { Thread.sleep(100);  } catch ( Exception e) { Main.log(e);  }
                } 
                mediaPlayer1.stop(); mediaPlayer1.release();
                mediaPlayer2.stop(); mediaPlayer2.release();
    	} }).start();
    } catch ( Exception e) { Main.log(e);  }}

    public void stop() { loop = false; }
}
//####################################################################
//####################################################################
class EventPrinter implements MidiEventListener {  
	int channel = 0 ; Receiver recv; SoftSynthesizer synth;  
    public EventPrinter(Receiver r, SoftSynthesizer s) { recv = r; synth=s; }
    @Override public void onStart(boolean fromBeginning) { }
    @Override public void onEvent(MidiEvent event, long ms) {try {

    	if(event instanceof ChannelEvent) 
    		channel = ((ChannelEvent) event).getChannel();
    	if (event instanceof ProgramChange) {ProgramChange e = (ProgramChange) event;
    		synth.getChannels()[e.getChannel()].programChange(e.getProgramNumber());
    	}if (event instanceof NoteOn) { NoteOn n = (NoteOn) event;  
    		sendMidiMsg(ShortMessage.NOTE_ON, channel, n.getNoteValue(), n.getVelocity(), ms); 
    	} else if (event instanceof NoteOff) { NoteOff n = (NoteOff) event; 
    		sendMidiMsg(ShortMessage.NOTE_OFF, channel, n.getNoteValue(), n.getVelocity(), ms);
    	} else if (event instanceof Controller) { Controller c = (Controller) event;
    		sendMidiMsg(ShortMessage.CONTROL_CHANGE, channel, c.getControllerType(), c.getValue(), ms);
    	} else if (event instanceof PitchBend) { PitchBend p = (PitchBend) event; 
    		sendMidiMsg(ShortMessage.PITCH_BEND, p.getChannel(), p.getLeastSignificantBits(), p.getMostSignificantBits(), ms); 
    	} else if (event instanceof ChannelAftertouch) { ChannelAftertouch c = (ChannelAftertouch) event;
    		sendMidiMsg(ShortMessage.CHANNEL_PRESSURE, channel, c.getType(), c.getAmount(), ms);
    	} else if (event instanceof NoteAftertouch) { NoteAftertouch n = (NoteAftertouch) event;
    		sendMidiMsg(ShortMessage.POLY_PRESSURE, channel, n.getNoteValue(), n.getAmount(), ms);
    	} else if (event instanceof Tempo) { Tempo tempo = (Tempo) event;
    		int mpqnValue = tempo.getMpqn();   
    		MetaMessage tempoMessage = new MetaMessage();
    		tempoMessage.setMessage(0x51, new byte[]{
    			(byte) ((mpqnValue >> 16) & 0xFF), (byte) ((mpqnValue >> 8) & 0xFF), (byte) (mpqnValue & 0xFF) }, 3);  
    		recv.send(tempoMessage, ms); 
    	}

    } catch ( Exception e) { Main.log(e); } }

    @Override public void onStop(boolean finished) {  Main.log("midi ended");Main.instance.soundPlayer.playOldSequence(); }
    
    void sendMidiMsg(int command, int channel, int data1, int data2, long timeStamp) { try {
        ShortMessage message = new ShortMessage();
        message.setMessage(command, channel, data1, data2);
        recv.send(message, timeStamp);
    } catch (Exception e) {  Main.log(e);  }  }
}