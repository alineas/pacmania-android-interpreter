package agame;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.example.kq7.Main;
import com.example.kq7.MainActivity;

import android.database.Cursor;
   

public class SQLLine {
	 
	final int id;
	final Map <String,String> datas;
	   
	
	//##########################################################################################
    public SQLLine(String table, Set<String> columns, int id, Map<String, String> rowData) {
        this.id = id;
        this.datas = new HashMap<>(rowData);
    }
	//##########################################################################################
	public String getString(String column) {
		String d = datas.get(column);
		if(d==null)return"";
		return d.replaceAll("`","'"); }
	//##########################################################################################
	public int getInt(String column) {  
		try {
			return Integer.parseInt(datas.get(column));
		} catch (NumberFormatException e) {
			return -998; //dont touch !!! i used this value cause there is no null check on an integer
		}   
	} 
	//##########################################################################################
	public int getId() {  return id; } 
	public Map<String, String> getDatas() { return datas; }
	 
	//##########################################################################################
	public String[] getArray(String column) {  
		String res = getString(column);
		 res=res.replaceAll(",", ";");
		if(!res.contains(";"))
			return new String[] {res};
		return res.replaceAll("^;", "").split(";");
	} 
	
	//########################################################################################## 
	public LinkedList<Integer> getList(String column) {
		LinkedList<Integer> temp = new LinkedList<Integer>();
		for(String a : getArray(column))
			if(a!=null&&!a.equals(""))
				temp.add(Integer.parseInt(a));
		return temp;
	}
	//######################################################################### 
 
	 
	 public Map<String,String> duoArray(String column) {
	        String[] array = getArray(column);
	        Map<String,String> temp = new HashMap<String, String>();  
			for(String l : array) { if(l.length()<2)continue;// empty "^;" 
				 temp.put( l.split("=")[0].replaceAll(" ", ""),l.split("=")[1].replaceAll(" ", "") );
			}
			return temp; 
	    }
	 public int getInt(String column, String key) {  
		 try {
			return Integer.parseInt(duoArray(column).get(key));
		} catch (NumberFormatException e) {
			 Utils.log( key+" was not found in column "+column);
		}
		return -998;
	            
	    } 
	
	 public String getString(String column, String key) { 
		 return  duoArray(column).get(key) ;
	    } 
}
